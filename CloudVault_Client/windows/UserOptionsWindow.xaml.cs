﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CloudVault_ClientCore.DataAccess.user.update;

namespace CloudVault_Client.windows
{
    /// <summary>
    /// Logique d'interaction pour UserOptionsWindow.xaml
    /// </summary>
    public partial class UserOptionsWindow : Window
    {
        public UserOptionsWindow()
        {
            InitializeComponent();
            Loaded += delegate
            {
                MinHeight = ActualHeight;
                MinWidth = ActualWidth;
            };
        }

        private async void OnClickLogout(object sender, RoutedEventArgs e)
        {
            if (WindowManager.CoreManager.IsConnected)
                await WindowManager.CoreManager.Disconnect();
            WindowManager.CoreManager.Logout();
            WindowManager.MainWindow.TaskbarIcon.Dispose();
            WindowManager.MainWindow.Close();
            WindowManager.Start();
        }

        private void ClickCancel(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private async void OnClickChangeEmail(object sender, RoutedEventArgs e)
        {
            if (IsValidEmail(TextBoxAccount.Text))
            {
                var success = await WindowManager.CoreManager.UserService.UpdateEmailUserAsync(new UserUpdateEmail
                {
                    Email = TextBoxAccount.Text
                });
                if (success)
                {
                    new DialogWindow("To validate your new email. Please click the link you received by email")
                    {
                        Owner = WindowManager.MainWindow,
                        WindowStartupLocation = WindowStartupLocation.CenterOwner
                    }.ShowDialog();
                    OnClickLogout(this, new RoutedEventArgs());
                }
                else
                {
                    new DialogWindow("An error occurred, maybe the new email is already taken?")
                    {
                        Owner = WindowManager.MainWindow,
                        WindowStartupLocation = WindowStartupLocation.CenterOwner
                    }.ShowDialog();
                }
            }
            else
            {
                new DialogWindow("Your email is invalid")
                {
                    Owner = WindowManager.MainWindow,
                    WindowStartupLocation = WindowStartupLocation.CenterOwner
                }.ShowDialog();
            }
        }

        bool IsValidEmail(string email)
        {
            try
            {
                return new System.Net.Mail.MailAddress(email).Address == email;
            }
            catch
            {
                return false;
            }
        }
    }
}
