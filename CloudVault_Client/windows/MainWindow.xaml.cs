﻿using CloudVault_Client.controls;
using CloudVault_Client.controls.data;
using CloudVault_ClientCore.BusinessManagement.manager;
using CloudVault_ClientCore.BusinessManagement.vault;
using CloudVault_ClientCore.Dbo.data;
using CloudVault_ClientCore.Dbo.metadata;
using CloudVault_ClientCore.Dbo.user;
using CloudVault_ClientCore.Dbo.vault;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using static CloudVault_Client.controls.MainListDataControl;

namespace CloudVault_Client.windows
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private bool _canCloseWindow = false;
        public MainWindow()
        {
            DataContext = this;
            InitializeComponent();
            OptionsBarControl.AccountButton.ToolTip =
                WindowManager.CoreManager.UserBusiness.User.Email;
        }

        public void NewPasswordPage()
        {
            MainPage.Content = new PasswordViewControl();
            DisplayButtonsOnCreate();
        }

        public void NewNotePage()
        {
            MainPage.Content = new NoteViewControl();
            DisplayButtonsOnCreate();
        }

        public void NewCardPage()
        {
            MainPage.Content = new CardViewControl();
            DisplayButtonsOnCreate();
        }

        private void DisplayButtonsOnCreate()
        {
            MainList.HideCurrentRow();
            CreateButton.Visibility = Visibility.Visible;
            CancelButton.Visibility = Visibility.Visible;
            DeleteButton.Visibility = Visibility.Hidden;
        }

        public void DisplayPasswordPage(Vault vault)
        {
            Task.Run(async delegate
            {
                if (!(await vault.GetData(WindowManager.CoreManager,
                    await WindowManager.CoreManager.UserBusiness.MasterPassword.Get(),
                    DataType.Password) is PasswordData data))
                    return;

                Application.Current.Dispatcher.Invoke(delegate
                {
                    var content = new PasswordViewControl
                    {
                        TitleTextBox = {Text = data.Title},
                        DescriptionTextBox = {Text = data.Description},
                        WebTextBox = {Text = data.Url},
                        IdTextBox = {Text = data.Username},
                        PasswordDotTextBox = {Password = data.Password},
                        NoteTextBox = {Text = data.Note}
                    };

                    MainPage.Content = content;
                    DisplayButtonsOnEdit();
                });
            });
        }

        public void DisplayNotePage(Vault vault)
        {
            Task.Run(async delegate
            {
                if (!(await vault.GetData(WindowManager.CoreManager,
                    await WindowManager.CoreManager.UserBusiness.MasterPassword.Get(),
                    DataType.Note) is NoteData data))
                    return;

                Application.Current.Dispatcher.Invoke(delegate
                {
                    var content = new NoteViewControl
                    {
                        TitleTextBox = {Text = data.Title},
                        DescriptionTextBox = {Text = data.Description},
                        NoteTextBox = {Text = data.Note}
                    };

                    MainPage.Content = content;
                    DisplayButtonsOnEdit();
                });
            });
        }

        public void DisplayCardPage(Vault vault)
        {
            Task.Run(async delegate
            {
                if (!(await vault.GetData(WindowManager.CoreManager,
                    await WindowManager.CoreManager.UserBusiness.MasterPassword.Get(),
                    DataType.CreditCard) is CreditCardData data)) return;

                Application.Current.Dispatcher.Invoke(delegate
                {
                    var content = new CardViewControl
                    {
                        TitleTextBox = {Text = data.Title},
                        DescriptionTextBox = {Text = data.Description},
                        CardNumbersTextBox = {Text = data.Numbers},
                        OwnerTextBox = {Text = data.Owner},
                        CardDate = {Text = data.ExpiryDate},
                        CvvTextBox = {Text = data.Cvv},
                        NoteTextBox = {Text = data.Note}
                    };

                    MainPage.Content = content;
                    DisplayButtonsOnEdit();
                });
            });
        }

        private void DisplayButtonsOnEdit()
        {
            CreateButton.Visibility = Visibility.Hidden;
            UpdateButton.Visibility = Visibility.Visible;
            CancelButton.Visibility = Visibility.Visible;
            DeleteButton.Visibility = Visibility.Visible;
        }

        private void CreateDataClick(object sender, RoutedEventArgs e)
        {
            switch (MainPage.Content)
            {
                case PasswordViewControl passwordViewControl:
                    CreatePassword(passwordViewControl);
                    break;
                case NoteViewControl noteViewControl:
                    CreateNote(noteViewControl);
                    break;
                case CardViewControl cardViewControl:
                    CreateCard(cardViewControl);
                    break;
            }
        }

        private bool CheckWrongTitle(string title)
        {
            if (string.IsNullOrEmpty(title))
            {
                new DialogWindow("Title must not be empty !")
                {
                    Owner = this,
                    WindowStartupLocation = WindowStartupLocation.CenterOwner
                }.ShowDialog();
                return true;
            }
            return false;
        }

        private void CreatePassword(PasswordViewControl passwordViewControl)
        {
            if (CheckWrongTitle(passwordViewControl.TitleTextBox.Text))
                return;

            var password = new PasswordData
            {
                Title = passwordViewControl.TitleTextBox.Text,
                Description = passwordViewControl.DescriptionTextBox.Text,
                Url = passwordViewControl.WebTextBox.Text,
                Username = passwordViewControl.IdTextBox.Text,
                Password = passwordViewControl.PasswordDotTextBox.Visibility == Visibility.Collapsed ?
                            passwordViewControl.PasswordCharTextBox.Text :
                            passwordViewControl.PasswordDotTextBox.Password,
                Note = passwordViewControl.NoteTextBox.Text
            };

            CancelDataClick(null, null);
            Task.Run(async delegate
            {
                await WindowManager.CoreManager.VaultManager
                    .AddData(password,
                        await WindowManager.CoreManager.UserBusiness.MasterPassword.Get());
                WindowManager.CoreManager.LoadVaults();
                await WindowManager.CoreManager.LoadMetaDatas();

                Application.Current.Dispatcher.Invoke(delegate
                {
                    WindowManager.MainWindow.OptionsBarControl.SyncAnimation.SyncCommand.Execute(null);
                });
            });
        }

        private void CreateNote(NoteViewControl noteViewControl)
        {
            if (CheckWrongTitle(noteViewControl.TitleTextBox.Text))
                return;

            var note = new NoteData
            {
                Title = noteViewControl.TitleTextBox.Text,
                Description = noteViewControl.DescriptionTextBox.Text,
                Note = noteViewControl.NoteTextBox.Text
            };

            CancelDataClick(null, null);

            Task.Run(async delegate
            {
                await WindowManager.CoreManager.VaultManager
                    .AddData(note,
                        await WindowManager.CoreManager.UserBusiness.MasterPassword.Get());
                WindowManager.CoreManager.LoadVaults();
                await WindowManager.CoreManager.LoadMetaDatas();

                Application.Current.Dispatcher.Invoke(delegate
                {
                    WindowManager.MainWindow.OptionsBarControl.SyncAnimation.SyncCommand.Execute(null);
                });
            });
        }

        private void CreateCard(CardViewControl cardViewControl)
        {
            if (CheckWrongTitle(cardViewControl.TitleTextBox.Text))
                return;

            var card = new CreditCardData
            {
                Title = cardViewControl.TitleTextBox.Text,
                Description = cardViewControl.DescriptionTextBox.Text,
                Numbers = cardViewControl.CardNumbersTextBox.Text,
                Owner = cardViewControl.OwnerTextBox.Text,
                ExpiryDate = cardViewControl.CardDate.Text,
                Cvv = cardViewControl.CvvTextBox.Text,
                Note = cardViewControl.NoteTextBox.Text
            };

            CancelDataClick(null, null);
            Task.Run(async delegate
            {
                await WindowManager.CoreManager.VaultManager
                    .AddData(card,
                        await WindowManager.CoreManager.UserBusiness.MasterPassword.Get());
                WindowManager.CoreManager.LoadVaults();
                await WindowManager.CoreManager.LoadMetaDatas();

                Application.Current.Dispatcher.Invoke(delegate
                {
                    WindowManager.MainWindow.OptionsBarControl.SyncAnimation.SyncCommand.Execute(null);
                });
            });
        }

        private void UpdateDataClick(object sender, RoutedEventArgs e)
        {
            var row = (Row)MainList.CurrentRow.DataContext;
            var vault = row.Vault;
            var metaData = row.MetaData;

            switch (MainPage.Content)
            {
                case PasswordViewControl passwordViewControl:
                    UpdatePassword(passwordViewControl, vault, metaData);
                    break;
                case NoteViewControl noteViewControl:
                    UpdateNote(noteViewControl, vault, metaData);
                    break;
                case CardViewControl cardViewControl:
                    UpdateCard(cardViewControl, vault, metaData);
                    break;
            }
        }

        private void UpdatePassword(PasswordViewControl passwordViewControl, Vault vault, MetaData metaData)
        {
            if (CheckWrongTitle(passwordViewControl.TitleTextBox.Text))
                return;

            var password = new PasswordData
            {
                Title = passwordViewControl.TitleTextBox.Text,
                Description = passwordViewControl.DescriptionTextBox.Text,
                Url = passwordViewControl.WebTextBox.Text,
                Username = passwordViewControl.IdTextBox.Text,
                Password = passwordViewControl.PasswordDotTextBox.Visibility == Visibility.Collapsed ?
                            passwordViewControl.PasswordCharTextBox.Text :
                            passwordViewControl.PasswordDotTextBox.Password,
                Note = passwordViewControl.NoteTextBox.Text
            };

            CancelDataClick(null, null);
            Task.Run(async delegate
            {
                await WindowManager.CoreManager.VaultManager
                .Modify(vault, password, metaData,
                        await WindowManager.CoreManager.UserBusiness.MasterPassword.Get());
                WindowManager.CoreManager.LoadVaults();
                await WindowManager.CoreManager.LoadMetaDatas();

                Application.Current.Dispatcher.Invoke(delegate
                {
                    WindowManager.MainWindow.OptionsBarControl.SyncAnimation.SyncCommand.Execute(null);
                });
            });
        }

        private void UpdateNote(NoteViewControl noteViewControl, Vault vault, MetaData metaData)
        {
            if (CheckWrongTitle(noteViewControl.TitleTextBox.Text))
                return;

            var note = new NoteData
            {
                Title = noteViewControl.TitleTextBox.Text,
                Description = noteViewControl.DescriptionTextBox.Text,
                Note = noteViewControl.NoteTextBox.Text
            };

            CancelDataClick(null, null);

            Task.Run(async delegate
            {
                await WindowManager.CoreManager.VaultManager
                    .Modify(vault, note, metaData,
                        await WindowManager.CoreManager.UserBusiness.MasterPassword.Get());
                WindowManager.CoreManager.LoadVaults();
                await WindowManager.CoreManager.LoadMetaDatas();

                Application.Current.Dispatcher.Invoke(delegate
                {
                    WindowManager.MainWindow.OptionsBarControl.SyncAnimation.SyncCommand.Execute(null);
                });
            });
        }

        private void UpdateCard(CardViewControl cardViewControl, Vault vault, MetaData metaData)
        {
            if (CheckWrongTitle(cardViewControl.TitleTextBox.Text))
                return;

            var card = new CreditCardData
            {
                Title = cardViewControl.TitleTextBox.Text,
                Description = cardViewControl.DescriptionTextBox.Text,
                Numbers = cardViewControl.CardNumbersTextBox.Text,
                Owner = cardViewControl.OwnerTextBox.Text,
                ExpiryDate = cardViewControl.CardDate.Text,
                Cvv = cardViewControl.CvvTextBox.Text,
                Note = cardViewControl.NoteTextBox.Text
            };

            CancelDataClick(null, null);
            Task.Run(async delegate
            {
                await WindowManager.CoreManager.VaultManager
                    .Modify(vault, card, metaData,
                        await WindowManager.CoreManager.UserBusiness.MasterPassword.Get());
                WindowManager.CoreManager.LoadVaults();
                await WindowManager.CoreManager.LoadMetaDatas();

                Application.Current.Dispatcher.Invoke(delegate
                {
                    WindowManager.MainWindow.OptionsBarControl.SyncAnimation.SyncCommand.Execute(null);
                });
            });
        }

        public void CancelDataClick(object sender, RoutedEventArgs e)
        {
            MainPage.Content = null;
            MainList.CancelItemSelect();
            DeleteButton.Visibility = Visibility.Hidden;
            CancelButton.Visibility = Visibility.Hidden;
            UpdateButton.Visibility = Visibility.Hidden;
            CreateButton.Visibility = Visibility.Hidden;
        }

        private void DeleteDataClick(object sender, RoutedEventArgs e)
        {
            if (WindowManager.MainWindow.MainList.DeleteCurrentItem())
                CancelDataClick(null, null);
        }

        public void MyClose()
        {
            if (_canCloseWindow)
                Close();
            else
                Hide();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            e.Cancel = _canCloseWindow;
            if (!_canCloseWindow)
                Hide();
        }

        private void TaskbarIcon_TrayMouseDoubleClick(object sender, RoutedEventArgs e)
        {
            if (!IsVisible)
                Show();
            if (!IsActive)
                Activate();
        }

        private void MenuItem_Quit_Click(object sender, RoutedEventArgs e)
        {
            Quit();
        }

        private void Quit()
        {
            Application.Current.Shutdown();
        }
    }
}
