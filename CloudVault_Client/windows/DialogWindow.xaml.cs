﻿using System.Windows;

namespace CloudVault_Client.windows
{
    /// <summary>
    /// Logique d'interaction pour DialogWindow.xaml
    /// </summary>
    public partial class DialogWindow : Window
    {
        public DialogWindow(string message)
        {
            InitializeComponent();
            MessageBox.Text = message;
        }

        private void OnOk(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
