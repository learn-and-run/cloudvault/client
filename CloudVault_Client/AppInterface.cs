﻿using System.Threading.Tasks;
using System.Windows;
using CloudVault_Client.windows;
using CloudVault_ClientCore.BusinessManagement.user;

namespace CloudVault_Client
{
    public class AppInterface : IPasswordAsker
    {
        public Task<string> AskPassword()
        {
            return Application.Current.Dispatcher.Invoke(delegate
            {
                var asker = new PasswordAskerWindow
                {
                    Owner = WindowManager.MainWindow,
                    WindowStartupLocation = WindowStartupLocation.CenterOwner
                };
                asker.Loaded += delegate
                {
                    asker.MinHeight = asker.ActualHeight;
                    asker.MinWidth = asker.ActualWidth;
                };
                asker.ShowDialog();
                return Task.FromResult(asker.AskerControl.ResultPassword);
            });
        }
    }
}
