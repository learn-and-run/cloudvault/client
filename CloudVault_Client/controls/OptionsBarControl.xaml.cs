﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using CloudVault_Client.windows;
using Microsoft.Xaml.Behaviors.Core;

namespace CloudVault_Client.controls
{
    /// <summary>
    /// Logique d'interaction pour OptionsBarControl.xaml
    /// </summary>
    public partial class OptionsBarControl : UserControl
    {
        public SyncAnim SyncAnimation { get; set; } = new SyncAnim();
        private bool _init = false;

        public OptionsBarControl()
        {
            
            DataContext = SyncAnimation;
            InitializeComponent();
            ToggleAskPass.IsChecked =
                WindowManager.CoreManager.UserBusiness.User.UserOptions.DelayForgotPasswordInSecond <= 60;
            ToggleRunAtStart.IsChecked =
                WindowManager.CoreManager.UserBusiness.User.UserOptions.StartWithComputer;
            WindowManager.RegisterInStartup(WindowManager.CoreManager.UserBusiness.User.UserOptions.StartWithComputer);
            _init = true;
        }

        private void OnUserOptionsClick(object sender, RoutedEventArgs e)
        {
            new UserOptionsWindow
            {
                Owner = WindowManager.MainWindow,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            }.ShowDialog();
        }

        private void RunAtStartupToggleButton_OnChecked(object sender, RoutedEventArgs e)
        {
            if (_init)
            {
                WindowManager.CoreManager.UserBusiness.User.UserOptions.StartWithComputer = true;
                WindowManager.RegisterInStartup(true);
                WindowManager.CoreManager.SaveUserSettings();
            }
        }

        private void RunAtStartupToggleButton_OnUnChecked(object sender, RoutedEventArgs e)
        {
            if (_init)
            {
                WindowManager.CoreManager.UserBusiness.User.UserOptions.StartWithComputer = false;
                WindowManager.RegisterInStartup(false);
                WindowManager.CoreManager.SaveUserSettings();
            }
        }

        private void AskPasswordEachTime_OnChecked(object sender, RoutedEventArgs e)
        {
            if (_init)
            {
                WindowManager.CoreManager.UserBusiness.User.UserOptions.DelayForgotPasswordInSecond = 60;
                WindowManager.CoreManager.UserBusiness.MasterPassword.RestartForgotTask();
                WindowManager.CoreManager.SaveUserSettings();
            }
        }

        private void AskPasswordEachTime_OnUnChecked(object sender, RoutedEventArgs e)
        {
            if (_init)
            {
                WindowManager.CoreManager.UserBusiness.User.UserOptions.DelayForgotPasswordInSecond = 24 * 60 * 60;
                WindowManager.CoreManager.UserBusiness.MasterPassword.RestartForgotTask();
                WindowManager.CoreManager.SaveUserSettings();
            }
        }

        public class SyncAnim : INotifyPropertyChanged
        {
            private bool _isSaving;
            public bool IsSaving
            {
                get => _isSaving;
                private set => MutateVerbose(ref _isSaving, value, RaisePropertyChanged());
            }

            private double _saveProgress;
            public double SaveProgress
            {
                get => _saveProgress;
                private set => MutateVerbose(ref _saveProgress, value, RaisePropertyChanged());
            }

            public ICommand SyncCommand { get; set; }
            public SyncAnim()
            {
                SyncCommand = new ActionCommand(_ =>
                {
                    if (Math.Abs(SaveProgress) > 0.01d) return;

                    var done = false;
                    Task.Run(async delegate
                    {
                        try
                        {
                            if (!WindowManager.CoreManager.IsConnected)
                                await WindowManager.CoreManager.Connect();
                            if (WindowManager.CoreManager.IsConnected)
                                if (await WindowManager.CoreManager.SynchronizeWithServer())
                                {
                                    WindowManager.CoreManager.LoadVaults();
                                    await WindowManager.CoreManager.LoadMetaDatas();
                                    done = true;
                                }
                        }
                        catch (Exception)
                        {
                            // ignore
                        }
                        finally
                        {
                            if (!done)
                            {
                                done = true;
                                Application.Current.Dispatcher.Invoke(delegate
                                {
                                    new DialogWindow("An error occured while synchronizing datas with the server!")
                                    {
                                        Owner = WindowManager.MainWindow,
                                        WindowStartupLocation = WindowStartupLocation.CenterOwner
                                    }.ShowDialog();
                                });
                            }
                        }
                    });
                    var started = DateTime.Now;
                    IsSaving = true;


                    new DispatcherTimer(
                        TimeSpan.FromMilliseconds(50),
                        DispatcherPriority.Normal,
                        new EventHandler((o, e) =>
                        {
                            var totalDuration = started.AddSeconds(5).Ticks - started.Ticks;
                            var currentProgress = DateTime.Now.Ticks - started.Ticks;
                            var currentProgressPercent = 100.0 / totalDuration * currentProgress;

                            SaveProgress = currentProgressPercent;

                            if (SaveProgress >= 100 && done)
                            {
                                IsSaving = false;
                                SaveProgress = 0;
                                ((DispatcherTimer)o).Stop();
                            }

                        }), Dispatcher.CurrentDispatcher);
                });
            }

            public event PropertyChangedEventHandler PropertyChanged;

            private Action<PropertyChangedEventArgs> RaisePropertyChanged()
            {
                return args => PropertyChanged?.Invoke(this, args);
            }

            public bool MutateVerbose<TField>(
                ref TField field,
                TField newValue,
                Action<PropertyChangedEventArgs> raise,
                [CallerMemberName] string propertyName = null)
            {
                if (EqualityComparer<TField>.Default.Equals(field, newValue)) return false;
                field = newValue;
                raise?.Invoke(new PropertyChangedEventArgs(propertyName));
                return true;
            }
        }
    }

}
