﻿using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using CloudVault_Client.windows;

namespace CloudVault_Client.controls
{
    /// <summary>
    /// Logique d'interaction pour PasswordAskerControl.xaml
    /// </summary>
    public partial class PasswordAskerControl : UserControl
    {
        public string ResultPassword { get; set; }
        public PasswordAskerControl()
        {
            InitializeComponent();

            FloatingPasswordBox.Loaded += delegate
            {
                FloatingPasswordBox.Focus();
            };
        }


        private void ShowPasswordCharsCheckBox_Checked(object sender, RoutedEventArgs e)
        {

            FloatingPasswordBox.Visibility = Visibility.Collapsed;
            MyTextBox.Visibility = Visibility.Visible;

            MyTextBox.Text = FloatingPasswordBox.Password;
            MyTextBox.CaretIndex = MyTextBox.Text.Length;
            MyTextBox.Focus();
        }

        private void ShowPasswordCharsCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            MyTextBox.Visibility = System.Windows.Visibility.Collapsed;
            FloatingPasswordBox.Visibility = System.Windows.Visibility.Visible;

            FloatingPasswordBox.Password = MyTextBox.Text;
            SetSelection(FloatingPasswordBox, MyTextBox.Text.Length, 0);
            FloatingPasswordBox.Focus();
        }

        private void SetSelection(PasswordBox passwordBox, int start, int length)
        {
            passwordBox.GetType()
                .GetMethod("Select", BindingFlags.Instance | BindingFlags.NonPublic)
                ?.Invoke(passwordBox, new object[] { start, length });
        }

        private async void OnClick_ButtonContinue(object sender, RoutedEventArgs e)
        {
            var password = FloatingPasswordBox.Password;
            if (ShowPasswordCharsCheckBox.IsChecked != null && (bool) ShowPasswordCharsCheckBox.IsChecked)
                password = MyTextBox.Text;

            var window = Window.GetWindow(this);
            if (window == null)
                return;
            if (await WindowManager.CoreManager.CheckLocalPassword(password) != null)
            {
                new DialogWindow("Wrong password!")
                {
                    Owner = window,
                    WindowStartupLocation = WindowStartupLocation.CenterOwner
                }.ShowDialog();
                return;
            }
            
            ResultPassword = password;
            window.DialogResult = true;
            window.Close();
        }

        private void OnClick_ButtonCancel(object sender, RoutedEventArgs e)
        {
            var window = Window.GetWindow(this);
            if (window == null)
                return;
            window.DialogResult = false;
            window.Close();
        }

        private void OnPressKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                OnClick_ButtonContinue(this, new RoutedEventArgs());
            }
        }
    }
}
