﻿using CloudVault_Client.objects;
using CloudVault_Client.windows;
using CloudVault_ClientCore.Dbo.data;
using CloudVault_ClientCore.Dbo.metadata;
using CloudVault_ClientCore.Dbo.vault;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace CloudVault_Client.controls
{
    /// <summary>
    /// Logique d'interaction pour MainListDataControl.xaml
    /// </summary>
    public partial class MainListDataControl : UserControl
    {
        private string SearchBarContent { get; set; } = "";
        private string CurrentTab { get; set; } = "All";
        public Grid CurrentRow { get; set; } = null;
        private ObservableCollection<Row> MyList { get; set; } = new ObservableCollection<Row>();

        public MainListDataControl()
        {
            InitializeComponent();
            WindowManager.CoreManager.EndLoadMetaDatasEvent += delegate
            {
                MyList = new ObservableCollection<Row>(WindowManager.CoreManager.GetMetaDatas()
                    .Select(keyValue => new Row(keyValue.Key, keyValue.Value))
                    .OrderBy(item => item.MetaData.Title)
                    .ThenBy(item => item.MetaData.Title));
                Application.Current.Dispatcher.Invoke(Refresh);
            };
        }

        public void HideCurrentRow()
        {
            if (CurrentRow != null)
                CurrentRow.Background = new SolidColorBrush(Color.FromArgb(0, 0, 0, 0));
        }

        private void DisplayCurrentRow()
        {
            if (CurrentRow != null)
                CurrentRow.Background = new SolidColorBrush(Color.FromArgb(128, 40, 180, 240));
        }

        private void SelectItemClick(object sender, MouseButtonEventArgs e)
        {
            HideCurrentRow();
            CurrentRow = (Grid)sender;
            DisplayCurrentRow();

            var row = (Row) CurrentRow.DataContext;
            switch (row.MetaData.DataType)
            {
                case DataType.Password:
                    WindowManager.MainWindow.DisplayPasswordPage(row.Vault);
                    break;
                case DataType.Note:
                    WindowManager.MainWindow.DisplayNotePage(row.Vault);
                    break;
                case DataType.CreditCard:
                    WindowManager.MainWindow.DisplayCardPage(row.Vault);
                    break;
            }
        }

        public void CancelItemSelect()
        {
            HideCurrentRow();
            CurrentRow = null;
        }

        public bool DeleteCurrentItem()
        {
            if (CurrentRow != null)
            {
                if (WindowManager.MainWindow.OptionsBarControl.SyncAnimation.IsSaving)
                {
                    new DialogWindow("Wait the end of the synchronization to delete some elements")
                    {
                        Owner = WindowManager.MainWindow,
                        WindowStartupLocation = WindowStartupLocation.CenterOwner
                    }.ShowDialog();
                    Delete.Visibility = Visibility.Collapsed;
                    return false; ;
                }

                var row = (Row)CurrentRow.DataContext;
                MyList.Remove(row);
                WindowManager.CoreManager.VaultManager.Delete(row.Vault);
                Refresh();

                WindowManager.MainWindow.OptionsBarControl.SyncAnimation.SyncCommand.Execute(null);

                return true;
            }

            return false;
        }

        private void SelectMultipleItemClick(object sender, RoutedEventArgs e)
        {
            if (MyList.Any(item => item.IsSelected))
            {
                Delete.Visibility = Visibility.Visible;
                return;
            }

            Delete.Visibility = Visibility.Collapsed;
        }

        private void DisplayAll(object sender, RoutedEventArgs e)
        {
            CurrentTab = "All";
            Refresh();
        }

        private void DisplayPassword(object sender, RoutedEventArgs e)
        {
            CurrentTab = "Password";
            Refresh();
        }

        private void DisplayNote(object sender, RoutedEventArgs e)
        {
            CurrentTab = "Note";
            Refresh();
        }

        private void DisplayCard(object sender, RoutedEventArgs e)
        {
            CurrentTab = "Card";
            Refresh();
        }

        private void Search(object sender, TextChangedEventArgs e)
        {
            SearchBarContent = ((TextBox) sender).Text;
            Refresh();
        }

        public void Refresh()
        {
            IEnumerable<Row> tmp;
            switch (CurrentTab)
            {
                case "All":
                    tmp = MyList;
                    break;
                case "Password":
                    tmp = MyList.Where(item => item.MetaData.DataType == DataType.Password);
                    break;
                case "Note":
                    tmp = MyList.Where(item => item.MetaData.DataType == DataType.Note);
                    break;
                case "Card":
                    tmp = MyList.Where(item => item.MetaData.DataType == DataType.CreditCard);
                    break;
                default:
                    tmp = new ObservableCollection<Row>();
                    break;
            }

            List.ItemsSource = SearchBarContent == "" ? tmp : tmp.Where(item => Match(item.MetaData));
        }

        private bool Match(MetaData metaData)
        {
            if (metaData.Title != null)
            {
                var t = metaData.Title.ToLower();
                if (t.Contains(SearchBarContent.ToLower()))
                    return true;
            }

            if (metaData.Description != null)
            {
                var d = metaData.Description.ToLower();
                if (d.Contains(SearchBarContent.ToLower()))
                    return true;
            }

            return false;
        }

        private void CreatePasswordClick(object sender, RoutedEventArgs e)
        {
            WindowManager.MainWindow.NewPasswordPage();
        }

        private void CreateNoteClick(object sender, RoutedEventArgs e)
        {
            WindowManager.MainWindow.NewNotePage();
        }

        private void CreateCardClick(object sender, RoutedEventArgs e)
        {
            WindowManager.MainWindow.NewCardPage();
        }

        private void DeleteClick(object sender, RoutedEventArgs e)
        {
            if (WindowManager.MainWindow.OptionsBarControl.SyncAnimation.IsSaving)
            {
                new DialogWindow("Wait the end of the synchronization to delete some elements")
                {
                    Owner = WindowManager.MainWindow,
                    WindowStartupLocation = WindowStartupLocation.CenterOwner
                }.ShowDialog();
                Delete.Visibility = Visibility.Collapsed;
                return;
            }

            Row row;
            while ((row = MyList.FirstOrDefault(item => item.IsSelected)) != null)
            {
                if (CurrentRow != null && ((Row)CurrentRow.DataContext).Vault.LocalGuid == row.Vault.LocalGuid)
                    WindowManager.MainWindow.CancelDataClick(null, null);
                MyList.Remove(row);
                WindowManager.CoreManager.VaultManager.Delete(row.Vault);
            }

            Delete.Visibility = Visibility.Collapsed;
            Refresh();

            WindowManager.MainWindow.OptionsBarControl.SyncAnimation.SyncCommand.Execute(null);
        }

        public class Row : SelectableViewModel
        {
            public Vault Vault { get; set; }
            public MetaData MetaData { get; set; }
            public string Type
            {
                get
                {
                    switch (MetaData.DataType)
                    {
                        case DataType.Password:
                            return "LockQuestion";
                        case DataType.Note:
                            return "NoteOutline";
                        case DataType.CreditCard:
                            return "CreditCardOutline";
                    }
                    return "help";
                }
            }

            public Row(Vault vault, MetaData metaData)
            {
                Vault = vault;
                MetaData = metaData;
            }
        }
    }
}
