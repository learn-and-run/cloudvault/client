﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using CloudVault_Client.windows;

namespace CloudVault_Client.controls
{
    /// <summary>
    /// Logique d'interaction pour WindowBarControl.xaml
    /// </summary>
    public sealed partial class WindowBarControl : UserControl
    {
        public Visibility IsCloseEnabled { get; set; }
        public Visibility IsReduceEnabled { get; set; }

        public static readonly DependencyProperty CloseClickEventDependency =
            DependencyProperty.Register("IsCloseEnabled", typeof(Visibility), typeof(WindowBarControl));
        public static readonly DependencyProperty ReduceClickEventDependency =
            DependencyProperty.Register("IsReduceEnabled", typeof(Visibility), typeof(WindowBarControl));

        public event CancelEventHandler CloseClickEvent;
        public event CancelEventHandler ReduceClickEvent;

        public WindowBarControl()
        {
            InitializeComponent();
            DataContext = this;
        }

        private void OnCloseButtonClick(object sender, RoutedEventArgs e)
        {
            var cancel = new CancelEventArgs();
            CloseClickEvent?.Invoke(this, cancel);
            if (!cancel.Cancel)
            {
                var window = Window.GetWindow(this);
                if (window is MainWindow mainWindow)
                    mainWindow.MyClose();
                else
                    window?.Close();
            }
        }

        private void OnMinimizeButtonClick(object sender, RoutedEventArgs e)
        {
            var cancel = new CancelEventArgs();
            ReduceClickEvent?.Invoke(this, cancel);
            if (!cancel.Cancel)
            {
                var window = Window.GetWindow(this);
                if (window != null)
                    window.WindowState = WindowState.Minimized;
            }
        }

        private void MouseDown_Drag(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton != MouseButton.Left) return;
            Window.GetWindow(this)?.DragMove();
        }
    }
}
