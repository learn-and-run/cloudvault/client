﻿using System.Windows.Controls;

namespace CloudVault_Client.controls.data
{
    /// <summary>
    /// Logique d'interaction pour NoteViewControl.xaml
    /// </summary>
    public partial class NoteViewControl : UserControl
    {
        public NoteViewControl()
        {
            InitializeComponent();
        }
    }
}
