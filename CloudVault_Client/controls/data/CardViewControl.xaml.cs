﻿using System.Windows.Controls;

namespace CloudVault_Client.controls.data
{
    /// <summary>
    /// Logique d'interaction pour CardViewControl.xaml
    /// </summary>
    public partial class CardViewControl : UserControl
    {
        public CardViewControl()
        {
            InitializeComponent();
        }
    }
}
