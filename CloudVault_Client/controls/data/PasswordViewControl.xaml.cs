﻿using System.Reflection;
using System.Windows;
using System.Windows.Controls;

namespace CloudVault_Client.controls.data
{
    /// <summary>
    /// Logique d'interaction pour PasswordViewControl.xaml
    /// </summary>
    public partial class PasswordViewControl : UserControl
    {
        public PasswordViewControl()
        {
            DataContext = this;
            InitializeComponent();
        }

        private void ShowPasswordCharsCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            PasswordDotTextBox.Visibility = Visibility.Collapsed;
            PasswordCharTextBox.Visibility = Visibility.Visible;

            PasswordCharTextBox.Text = PasswordDotTextBox.Password;
            PasswordCharTextBox.CaretIndex = PasswordCharTextBox.Text.Length;
            PasswordCharTextBox.Focus();
        }

        private void ShowPasswordCharsCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            PasswordCharTextBox.Visibility = System.Windows.Visibility.Collapsed;
            PasswordDotTextBox.Visibility = System.Windows.Visibility.Visible;

            PasswordDotTextBox.Password = PasswordCharTextBox.Text;
            SetSelection(PasswordDotTextBox, PasswordCharTextBox.Text.Length, 0);
            PasswordDotTextBox.Focus();
        }

        private void SetSelection(PasswordBox passwordBox, int start, int length)
        {
            passwordBox.GetType()
                .GetMethod("Select", BindingFlags.Instance | BindingFlags.NonPublic)
                ?.Invoke(passwordBox, new object[] { start, length });
        }
    }
}
