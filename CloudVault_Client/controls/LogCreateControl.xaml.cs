﻿using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using CloudVault_Client.windows;
using CloudVault_ClientCore.BusinessManagement.user;
using CloudVault_ClientCore.Dbo.user;
using MaterialDesignColors;
using MaterialDesignThemes.MahApps;
using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Converters;

namespace CloudVault_Client.controls
{
    /// <summary>
    /// Logique d'interaction pour LogCreateControl.xaml
    /// </summary>
    public partial class LogCreateControl : UserControl
    {
        public LogCreateControl()
        {
            InitializeComponent();
            Loaded += delegate
            {
                IdTextBox.Focus();
            };
        }

        private void ShowPasswordCharsCheckBox_Checked(object sender, RoutedEventArgs e)
        {

            FloatingPasswordBox.Visibility = Visibility.Collapsed;
            MyTextBox.Visibility = Visibility.Visible;

            MyTextBox.Text = FloatingPasswordBox.Password;
            MyTextBox.CaretIndex = MyTextBox.Text.Length;
            MyTextBox.Focus();
        }

        private void ShowPasswordCharsCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            MyTextBox.Visibility = System.Windows.Visibility.Collapsed;
            FloatingPasswordBox.Visibility = System.Windows.Visibility.Visible;

            FloatingPasswordBox.Password = MyTextBox.Text;
            SetSelection(FloatingPasswordBox, MyTextBox.Text.Length, 0);
            FloatingPasswordBox.Focus();
        }

        private void SetSelection(PasswordBox passwordBox, int start, int length)
        {
            passwordBox.GetType()
                .GetMethod("Select", BindingFlags.Instance | BindingFlags.NonPublic)
                .Invoke(passwordBox, new object[] { start, length });
        }

        private async void OnClick_ButtonCreate(object sender, RoutedEventArgs e)
        {
            var email = IdTextBox.Text;
            if (!IsValidEmail(email))
            {
                new DialogWindow("Your email is invalid")
                {
                    Owner = Window.GetWindow(this),
                    WindowStartupLocation = WindowStartupLocation.CenterOwner
                }.ShowDialog();
                return;
            }
            var password = FloatingPasswordBox.Visibility == Visibility.Collapsed ? MyTextBox.Text : FloatingPasswordBox.Password;
            if (!CheckPassword(password))
            {
                new DialogWindow("Your password does not match the rules!")
                {
                    Owner = Window.GetWindow(this),
                    WindowStartupLocation = WindowStartupLocation.CenterOwner
                }.ShowDialog();
                return;
            }
            var user = new User(email);
            var userBusiness = new UserBusiness(user, WindowManager.AppInterface, password);
            var result = await WindowManager.CoreManager.CreateUser(userBusiness);
            if (result == false)
            {
                new DialogWindow("An error occurred! Your email may be already taken!")
                {
                    Owner = Window.GetWindow(this),
                    WindowStartupLocation = WindowStartupLocation.CenterOwner
                }.ShowDialog();
                return;
            }
            new DialogWindow("Your account has been created successfully!\nPlease confirm your email and then login")
            {
                Owner = Window.GetWindow(this),
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            }.ShowDialog();
        }
        bool IsValidEmail(string email)
        {
            try
            {
                return new System.Net.Mail.MailAddress(email).Address == email;
            }
            catch
            {
                return false;
            }
        }

        private async void OnClick_ButtonLogin(object sender, RoutedEventArgs e)
        {
            var email = IdTextBox.Text;
            if (!IsValidEmail(email))
            {
                new DialogWindow("Your email is invalid")
                {
                    Owner = Window.GetWindow(this),
                    WindowStartupLocation = WindowStartupLocation.CenterOwner
                }.ShowDialog();
                return;
            }
            var password = FloatingPasswordBox.Visibility == Visibility.Collapsed ? MyTextBox.Text : FloatingPasswordBox.Password;
            if (!CheckPassword(password))
            {
                new DialogWindow("Your password does not match the rules!")
                {
                    Owner = Window.GetWindow(this),
                    WindowStartupLocation = WindowStartupLocation.CenterOwner
                }.ShowDialog();
                return;
            }
            var user = new User(email);
            var userBusiness = new UserBusiness(user, WindowManager.AppInterface, password);
            var result = await WindowManager.CoreManager.Login(userBusiness);
            if (result == false)
            {
                new DialogWindow("An error occurred! Please check your credentials")
                {
                    Owner = Window.GetWindow(this),
                    WindowStartupLocation = WindowStartupLocation.CenterOwner
                }.ShowDialog();
                return;
            }
            Window.GetWindow(this)?.Close();
        }

        private bool? _passState = null;
        private void OnPasswordChanged(object sender, RoutedEventArgs e)
        {
            _passState = FloatingPasswordBox.Visibility == Visibility.Collapsed ?
                CheckPassword(MyTextBox.Text) :
                CheckPassword(FloatingPasswordBox.Password);
            if (_passState == true)
            {
                TextFieldAssist.SetUnderlineBrush(MyTextBox, new SolidColorBrush(Color.FromArgb(255, 0, 128, 0)));
                TextFieldAssist.SetUnderlineBrush(FloatingPasswordBox, new SolidColorBrush(Color.FromArgb(255, 0, 128, 0)));
            }
            else if (_passState == false)
            {
                TextFieldAssist.SetUnderlineBrush(MyTextBox, new SolidColorBrush(Color.FromArgb(255, 128, 0, 0)));
                TextFieldAssist.SetUnderlineBrush(FloatingPasswordBox, new SolidColorBrush(Color.FromArgb(255, 128, 0, 0)));
            }
            //FloatingPasswordBox.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 193, 7));
        }

        private bool CheckPassword(string password)
        {
            var hasNumber = new Regex(@"[0-9]+");
            var hasLowerChar = new Regex(@"[a-z]+");
            var hasUpperChar = new Regex(@"[A-Z]+");
            var hasMinimum10Chars = new Regex(@".{10,}");

            return hasNumber.IsMatch(password) &&
                   hasUpperChar.IsMatch(password) &&
                   hasLowerChar.IsMatch(password) &&
                   hasMinimum10Chars.IsMatch(password);
        }

        private void OnPressEnter(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                OnClick_ButtonLogin(this, new RoutedEventArgs());
        }
    }
}
