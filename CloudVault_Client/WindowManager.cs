﻿using System.IO;
using System.Threading.Tasks;
using System.Windows;
using CloudVault_Client.windows;
using CloudVault_ClientCore.BusinessManagement.manager;
using Microsoft.Win32;

namespace CloudVault_Client
{
    public static class WindowManager
    {
        public static CoreManager CoreManager { get; set; }
        public static AppInterface AppInterface { get; set; }
        public static MainWindow MainWindow { get; set; }

        public static void Start()
        {
            CoreManager = new CoreManager();
            var host = "https://jeanjacquelin.fr";
            if (!File.Exists("host"))
                File.WriteAllText("host", host);
            else
                host = File.ReadAllText("host");
            CoreManager.Init(host);
            AppInterface = new AppInterface();

            var loginWindow = new LoginWindow()
            {
                WindowStartupLocation = WindowStartupLocation.CenterScreen
            };
            loginWindow.Loaded += delegate
            {
                loginWindow.MinHeight = loginWindow.ActualHeight;
                loginWindow.MinWidth = loginWindow.ActualWidth;
            };
            loginWindow.ShowDialog();
            
            if (!CoreManager.IsLogged)
            {
                Application.Current.Shutdown(0);
                return;
            }

            MainWindow = new MainWindow
            {
                WindowStartupLocation = WindowStartupLocation.CenterScreen
            };

            Connect();

            MainWindow.Loaded += delegate
            {
                 MainWindow.Height = MainWindow.MinHeight;
                 MainWindow.Width = MainWindow.MinWidth;
            };
            MainWindow.Show();
        }

        public static void Connect()
        {
            Task.Run(async delegate
            {
                await CoreManager.Connect();
                CoreManager.LoadVaults();
                await CoreManager.LoadMetaDatas();

                Application.Current.Dispatcher.Invoke(delegate
                {
                    MainWindow.OptionsBarControl.SyncAnimation
                        .SyncCommand.Execute(null);
                });
            });
        }

        public static void RegisterInStartup(bool isChecked)
        {
            var registryKey = Registry.CurrentUser.OpenSubKey
                ("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            if (isChecked)
            {
                registryKey?.SetValue("ApplicationName", System.Reflection.Assembly.GetExecutingAssembly().Location);
            }
            else
            {
                registryKey?.DeleteValue("ApplicationName");
            }
        }
    }
}
