﻿using CloudVault_ClientCore.Dbo.user;
using CloudVault_ClientCore.Dbo.user.options;

namespace TestClient.Fake
{
    public class FakeUser : IUser
    {
        public string Email { get; set; }
        public UserOptions UserOptions { get; set; }
    }
}
