﻿using System;
using System.Threading.Tasks;
using CloudVault_ClientCore.BusinessManagement.user;

namespace TestClient.Fake
{
    public class FakePasswordAsker : IPasswordAsker
    {
        private readonly string _password;
        public Action NotifyAction { get; set; } = () => { };
        public Task<string> AskPassword()
        {
            NotifyAction();
            return Task.FromResult(_password);
        }
        public FakePasswordAsker(string password)
        {
            this._password = password;
        }
    }
}
