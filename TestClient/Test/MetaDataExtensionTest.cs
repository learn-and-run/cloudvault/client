﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CloudVault_ClientCore.Dbo.data;
using CloudVault_ClientCore.Dbo.metadata;
using CloudVault_ClientCore.BusinessManagement.metadata;
using Org.BouncyCastle.Asn1.X509;

namespace TestClient.Test
{
    [TestClass]
    public class MetaDataExtensionTest
    {
        [TestMethod]
        public void TestWithoutUpdateFrom()
        {
            var noteData = new NoteData();

            var metaData = new MetaData(noteData);

            noteData.Title = "New Title";
            noteData.Description = "New description";
            noteData.AskPasswordEachTime = true;

            Assert.AreNotEqual(noteData.Title, metaData.Title);
            Assert.AreNotEqual(noteData.Description, metaData.Description);
            Assert.AreNotEqual(noteData.AskPasswordEachTime, metaData.AskPasswordEachTime);
        }

        [TestMethod]
        public void TestUpdateFrom()
        {
            var noteData = new NoteData();

            var metaData = new MetaData(noteData);

            noteData.Title = "New Title";
            noteData.Description = "New description";
            noteData.AskPasswordEachTime = true;

            metaData.UpdateFrom(noteData);

            Assert.AreEqual(noteData.Title, metaData.Title);
            Assert.AreEqual(noteData.Description, metaData.Description);
            Assert.AreEqual(noteData.AskPasswordEachTime, metaData.AskPasswordEachTime);
        }
    }
}
