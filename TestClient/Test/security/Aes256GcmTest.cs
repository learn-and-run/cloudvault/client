﻿using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CloudVault_ClientCore.BusinessManagement.security;

namespace TestClient.Test.security
{
    [TestClass]
    public class Aes256GcmTest
    {
        private const string contentToEncryt = "Content to encrypt!";
        private const string password = "YesTh!S!SM?Pa55W0RD!";

        [TestMethod]
        public async Task TestFastEncryptWithPasswordReturnTrue()
        {
            var (success, _, _) = await Aes256Gcm.FastEncryptWithPassword(
                contentToEncryt,
                password
            );
            Assert.IsTrue(success);
        }

        [TestMethod]
        public async Task TestFastEncryptDecryptWithPassWordReturnTrue()
        {
            var (success, cypherText, _) = await Aes256Gcm.FastEncryptWithPassword(
                contentToEncryt,
                password
            );
            Assert.IsTrue(success);
            var (succeed, _, _) = await Aes256Gcm.FastDecryptWithPassword(
                cypherText,
                password
            );
            Assert.IsTrue(succeed);
        }

        [TestMethod]
        public async Task TestFastEncryptDecryptWithPassWordWorksTogether()
        {
            var (success, cypherText, _) = await Aes256Gcm.FastEncryptWithPassword(
                contentToEncryt,
                password
            );
            Assert.IsTrue(success);
            var (succeed, returnText, _) = await Aes256Gcm.FastDecryptWithPassword(
                cypherText,
                password
            );
            Assert.IsTrue(succeed);

            Assert.AreEqual(contentToEncryt, returnText);
        }
    }
}
