﻿using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CloudVault_ClientCore.BusinessManagement.security;
using System.IO;

namespace TestClient.Test.security
{
    [TestClass]
    public class CipherTest
    {
        private const string masterPassword = "Master!Password";
        private const string contentToSerialize = "I'm the content to serialize!";

        [TestMethod]
        public async Task TestCipherSerializationDeserialisation()
        {
            byte[] serialisation = await Cipher.SerializeAsync<string>(contentToSerialize, masterPassword);
            string result = await Cipher.DeserializeAsync<string>(serialisation, masterPassword);

            Assert.AreEqual(contentToSerialize, result);
        }

        [TestMethod]
        public async Task TestCipherSaveLoadAsync()
        {
            string filePath = Path.GetTempFileName();

            await Cipher.SaveAsync<string>(contentToSerialize, filePath, masterPassword);
            string result = await Cipher.LoadAsync<string>(filePath, masterPassword);

            File.Delete(filePath);

            Assert.AreEqual(contentToSerialize, result);
        }
    }
}
