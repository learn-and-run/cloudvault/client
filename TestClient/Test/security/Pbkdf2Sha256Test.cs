﻿using CloudVault_ClientCore.BusinessManagement.security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace TestClient.Test.security
{
    [TestClass]
    public class Pbkdf2Sha256Test
    {
        [TestMethod]
        public void TestGetKeyFromPasswordTooSmall()
        {
            Assert.ThrowsException<ArgumentException>(() =>
                Pbkdf2Sha256.GetKeyFromPassword("toosmall", out _)
            );
        }

        [TestMethod]
        public void TestCreateSaltHasGoodSize0()
        {
            int expected = 0;
            byte[] salt = Pbkdf2Sha256.CreateSalt(expected);
            Assert.AreEqual(expected, salt.Length);
        }

        [TestMethod]
        public void TestCreateSaltHasGoodSize2048()
        {
            int expected = 2048;
            byte[] salt = Pbkdf2Sha256.CreateSalt(expected);
            Assert.AreEqual(expected, salt.Length);
        }
    }
}
