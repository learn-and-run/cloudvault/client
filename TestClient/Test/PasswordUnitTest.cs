﻿using System.Threading.Tasks;
using CloudVault_ClientCore.BusinessManagement.user;
using CloudVault_ClientCore.Dbo.user;
using CloudVault_ClientCore.Dbo.user.options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestClient.Fake;

namespace TestClient.Test
{
    [TestClass]
    public class PasswordUnitTest
    {
        private const string Password = "M1ch3l";

        [TestMethod]
        public void MultiAskPasswordWithWait()
        {
            Task.Run(async () =>
            {
                var user = new User("a@a.com")
                {
                    UserOptions = new UserOptions()
                    {
                        DelayForgotPasswordInSecond = 5
                    }
                };
                var fakeAsker = new FakePasswordAsker(Password);
                var masterPassword = new MasterPassword(user, Password, fakeAsker);
                Assert.AreEqual(Password, await masterPassword.Get());
            }).GetAwaiter().GetResult();
        }
    }
}
