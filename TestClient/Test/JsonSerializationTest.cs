﻿using System;
using CloudVault_ClientCore.Dto.vault;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace TestClient.Test
{
    [TestClass]
    public class JsonSerializationTest
    {

        [TestMethod]
        public void VaultTest()
        {
            var vaultDto = new VaultDto(Guid.NewGuid().ToString(), 12);
            var json = JsonConvert.SerializeObject(vaultDto);

            var vaultDto2 = JsonConvert.DeserializeObject<VaultDto>(json);

            Assert.AreEqual(vaultDto, vaultDto2);
        }
    }
}
