﻿using System;
using AutoMapper;
using CloudVault_ClientCore.BusinessManagement.utils;
using CloudVault_ClientCore.Dbo.data;
using CloudVault_ClientCore.Dbo.metadata;
using CloudVault_ClientCore.Dbo.user;
using CloudVault_ClientCore.Dbo.user.options;
using CloudVault_ClientCore.Dbo.vault;
using CloudVault_ClientCore.Dto.metadata;
using CloudVault_ClientCore.Dto.user;
using CloudVault_ClientCore.Dto.vault;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestClient.Test
{
    [TestClass]
    public class MapperUnitTest
    {
        private IMapper Mapper { get; set; }

        [TestInitialize]
        public void Init()
        {
            Mapper = CloudVaultMapperProfile.GenerateDefaultMapper();
        }

        [TestMethod]
        public void VaultMap()
        {
            var vault = new Vault(Guid.NewGuid()) { ServerId = 12 };
            var vaultDto = Mapper.Map<VaultDto>(vault);

            var vaultDto2 = new VaultDto(vault.LocalGuid, vault.ServerId);

            Assert.AreEqual(vaultDto2, vaultDto);
        }

        [TestMethod]
        public void VaultMapReverse()
        {
            var guid = Guid.NewGuid();
            var vaultDto = new VaultDto(guid.ToString(), 12);
            var vault = Mapper.Map<Vault>(vaultDto);
            var expectedVault = new Vault(guid) { ServerId = 12 };

            Assert.AreEqual(expectedVault, vault);
        }

        [TestMethod]
        public void UserMap()
        {
            var user1 = new User("nico.acart@gmail.com")
            {
                UserOptions = new UserOptions()
                {
                    StartupMode = StartupMode.Hidden,
                    DelayForgotPasswordInSecond = 2,
                    StartWithComputer = true,
                    SynchronizeWithServer = true,
                    ThemeType = ThemeType.Dark
                }
            };
            var userDto = Mapper.Map<UserDto>(user1);
            var expectedDto = new UserDto()
            {
                Email = "nico.acart@gmail.com",
                UserOptionsStartupMode = StartupMode.Hidden,
                UserOptionsDelayForgotPasswordInSecond = 2,
                UserOptionsStartWithComputer = true,
                UserOptionsSynchronizeWithServer = true,
                UserOptionsThemeType = ThemeType.Dark
            };
            Assert.IsTrue(expectedDto.Email.Equals(userDto.Email));
            Assert.AreEqual(expectedDto.UserOptionsStartupMode, userDto.UserOptionsStartupMode);
            Assert.AreEqual(expectedDto.UserOptionsDelayForgotPasswordInSecond, userDto.UserOptionsDelayForgotPasswordInSecond);
            Assert.AreEqual(expectedDto.UserOptionsStartWithComputer, userDto.UserOptionsStartWithComputer);
            Assert.AreEqual(expectedDto.UserOptionsSynchronizeWithServer, userDto.UserOptionsSynchronizeWithServer);
            Assert.AreEqual(expectedDto.UserOptionsThemeType, userDto.UserOptionsThemeType);
        }

        [TestMethod]
        public void UserMapReverse()
        {
            var userDto = new UserDto()
            {
                Email = "nico.acart@gmail.com",
                UserOptionsStartupMode = StartupMode.Hidden,
                UserOptionsDelayForgotPasswordInSecond = 2,
                UserOptionsStartWithComputer = true,
                UserOptionsSynchronizeWithServer = true,
                UserOptionsThemeType = ThemeType.Dark
            };
            var user = Mapper.Map<User>(userDto);
            var expectedUser = new User("nico.acart@gmail.com")
            {
                UserOptions = new UserOptions()
                {
                    StartupMode = StartupMode.Hidden,
                    DelayForgotPasswordInSecond = 2,
                    StartWithComputer = true,
                    SynchronizeWithServer = true,
                    ThemeType = ThemeType.Dark
                }
            };
            Assert.IsTrue(expectedUser.Email.Equals(user.Email));
            Assert.AreEqual(expectedUser.UserOptions.StartupMode, user.UserOptions.StartupMode);
            Assert.AreEqual(expectedUser.UserOptions.DelayForgotPasswordInSecond, user.UserOptions.DelayForgotPasswordInSecond);
            Assert.AreEqual(expectedUser.UserOptions.StartWithComputer, user.UserOptions.StartWithComputer);
            Assert.AreEqual(expectedUser.UserOptions.SynchronizeWithServer, user.UserOptions.SynchronizeWithServer);
            Assert.AreEqual(expectedUser.UserOptions.ThemeType, user.UserOptions.ThemeType);
        }

        [TestMethod]
        public void MetaDataMap()
        {
            var data = new NoteData()
            {
                Title = "title",
                Note = "note",
                AskPasswordEachTime = false,
                Description = "desc"
            };
            var metaData = new MetaData(data);
            var metaDataDto = Mapper.Map<MetaDataDto>(metaData);

            var expectedMetaDataDto= new MetaDataDto()
            {
                Title = metaData.Title,
                AskPasswordEachTime = metaData.AskPasswordEachTime,
                Description = metaData.Description,
                LastUpdate = metaData.LastUpdate,
                DataType = metaData.DataType
            };
            Assert.AreEqual(expectedMetaDataDto.Title, metaDataDto.Title);
            Assert.AreEqual(expectedMetaDataDto.AskPasswordEachTime, metaDataDto.AskPasswordEachTime);
            Assert.AreEqual(expectedMetaDataDto.Description, metaDataDto.Description);
            Assert.AreEqual(expectedMetaDataDto.LastUpdate, metaDataDto.LastUpdate);
            Assert.AreEqual(expectedMetaDataDto.DataType, metaDataDto.DataType);
        }

        [TestMethod]
        public void MetaDataMapReverse()
        {
            var metaDataDto = new MetaDataDto()
            {
                Title = "title",
                Description = "desc",
                DataType = DataType.Note,
                LastUpdate = DateTime.Now,
                AskPasswordEachTime = false
            };
            var metaData = Mapper.Map<MetaData>(metaDataDto);
            var data = new NoteData()
            {
                Title = "title",
                Note = "note",
                AskPasswordEachTime = false,
                Description = "desc"
            };
            var expectedMetaData = new MetaData(data)
            {
                LastUpdate = metaDataDto.LastUpdate
            };
            Assert.AreEqual(expectedMetaData.Title, metaData.Title);
            Assert.AreEqual(expectedMetaData.AskPasswordEachTime, metaData.AskPasswordEachTime);
            Assert.AreEqual(expectedMetaData.Description, metaData.Description);
            Assert.AreEqual(expectedMetaData.LastUpdate, metaData.LastUpdate);
            Assert.AreEqual(expectedMetaData.DataType, metaData.DataType);
        }


    }
}
