﻿using System.Net.Http;
using System.Threading.Tasks;
using CloudVault_ClientCore.DataAccess.update;

namespace CloudVault_ClientCore.BusinessManagement.service
{
    public class UpdateService : AbstractService
    {
        public UpdateService(HttpClient httpClient) : base(httpClient) {}
        
        private const string UpdateDataRequest = "/api/data/update";
        
        public async Task<UpdateResponse> UpdateDataAsync(UpdateBody updateBody)
        {
            var response = await HttpClient.PutAsJsonAsync(UpdateDataRequest, updateBody);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsAsync<UpdateResponse>();
        }

    }
}