﻿using System.Net.Http;
using System.Threading.Tasks;

namespace CloudVault_ClientCore.BusinessManagement.service
{
    public abstract class AbstractService
    {
        protected HttpClient HttpClient { get; }

        protected AbstractService(HttpClient httpClient)
        {
            HttpClient = httpClient;
        }
        protected async Task<TResult> ApiUserCallAsync<TResult, TBody>(TBody body, string request)
        {
            var response = await HttpClient.PostAsJsonAsync(request, body);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsAsync<TResult>();
        }
    }
}