﻿using System.Net.Http;
using System.Threading.Tasks;
using CloudVault_ClientCore.DataAccess.user.create;
using CloudVault_ClientCore.DataAccess.user.login;
using CloudVault_ClientCore.DataAccess.user.update;

namespace CloudVault_ClientCore.BusinessManagement.service
{
    public class UserService : AbstractService
    {
        public UserService(HttpClient httpClient) : base(httpClient) {}
        
        private const string CreateUserRequest = "/api/user/create";
        private const string FinalizeUserRequest = "/api/user/finalize";
        private const string AskMsgUserRequest = "/api/user/msg";
        private const string LoginUserRequest = "/api/user/login";
        private const string LogoutUserRequest = "/api/user/logout";
        private const string UpdateEmailUserRequest = "/api/user/update_email";

        
        public async Task<UserCreateResponse> CreateUserAsync(UserCreateBody userCreateBody)
        {
            return await ApiUserCallAsync<UserCreateResponse, UserCreateBody>(userCreateBody, CreateUserRequest);
        }
        
        public async Task<bool> FinalizeUserAsync(UserFinalizeBody userFinalizeBody)
        {
            var response = await HttpClient.PostAsJsonAsync(FinalizeUserRequest, userFinalizeBody);
            return response.IsSuccessStatusCode;
        }
        
        public async Task<bool> LogoutUserAsync()
        {
            var response = await HttpClient.DeleteAsync(LogoutUserRequest);
            return response.IsSuccessStatusCode;
        }
        
        public async Task<bool> UpdateEmailUserAsync(UserUpdateEmail userUpdateEmail)
        {
            var response = await HttpClient.PutAsJsonAsync(UpdateEmailUserRequest, userUpdateEmail);
            return response.IsSuccessStatusCode;
        }
        
        public async Task<AskMsgResponse> AskMsgUserAsync(AskMsgBody askMsgBody)
        {
            return await ApiUserCallAsync<AskMsgResponse, AskMsgBody>(askMsgBody, AskMsgUserRequest);
        }
        
        public async Task<bool> LoginUserAsync(UserLoginBody userLoginBody)
        {
            var response = await HttpClient.PostAsJsonAsync(LoginUserRequest, userLoginBody);
            return response.IsSuccessStatusCode;
        }
        
    }
}