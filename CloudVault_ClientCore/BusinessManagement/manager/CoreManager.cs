﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security;
using System.Threading.Tasks;
using System.Xml.Linq;
using AutoMapper;
using CloudVault_ClientCore.BusinessManagement.security;
using CloudVault_ClientCore.BusinessManagement.service;
using CloudVault_ClientCore.BusinessManagement.user;
using CloudVault_ClientCore.BusinessManagement.utils;
using CloudVault_ClientCore.BusinessManagement.vault;
using CloudVault_ClientCore.DataAccess.update;
using CloudVault_ClientCore.DataAccess.user.create;
using CloudVault_ClientCore.DataAccess.user.login;
using CloudVault_ClientCore.Dbo.metadata;
using CloudVault_ClientCore.Dbo.user.options;
using CloudVault_ClientCore.Dbo.vault;
using CloudVault_ClientCore.Dto.manager;
using CloudVault_ClientCore.Dto.user;
using ErrorEventHandler = CloudVault_ClientCore.BusinessManagement.utils.ErrorEventHandler;

namespace CloudVault_ClientCore.BusinessManagement.manager
{
    public class CoreManager : ICoreManager
    {
        public string BaseDirectory { get; }
        public IMapper Mapper { get; private set; }
        public UserBusiness UserBusiness { get; private set; }
        public VaultManager VaultManager { get; private set; }
        public UserService UserService { get; private set; }
        public UpdateService UpdateService { get; private set; }
        public string SettingsFilePath => $"{BaseDirectory}/{GetUserDirectoryFunc(UserBusiness)}/settings.json";

        #region State
        public bool IsInitialized { get; set; }
        public bool IsVaultsLoaded { get; set; }
        public bool IsMetaDataLoaded { get; set; }
        public bool IsLogged { get; set; }
        public bool IsConnected { get; set; }
        #endregion State

        #region Events
        public event CancelEventHandler StartInitEvent;
        public event ErrorEventHandler ErrorInitEvent;
        public event EventHandler EndInitEvent;
        
        public event CancelEventHandler StartLoadVaultsEvent;
        public event ErrorEventHandler ErrorLoadVaultsEvent;
        public event EventHandler EndLoadVaultsEvent;
        
        public event CancelEventHandler StartLoadMetaDatasEvent;
        public event ErrorEventHandler ErrorLoadMetaDatasEvent;
        public event EventHandler EndLoadMetaDatasEvent;
        
        public event CancelEventHandler StartLoginEvent;
        public event ErrorEventHandler ErrorLoginEvent;
        public event EventHandler EndLoginEvent;
        
        public event CancelEventHandler StartConnectEvent;
        public event ErrorEventHandler ErrorConnectEvent;
        public event EventHandler EndConnectEvent;
        
        public event CancelEventHandler StartDisconnectEvent;
        public event ErrorEventHandler ErrorDisconnectEvent;
        public event EventHandler EndDisconnectEvent;
        
        public event CancelEventHandler StartLogoutEvent;
        public event ErrorEventHandler ErrorLogoutEvent;
        public event EventHandler EndLogoutEvent;
        
        public event CancelEventHandler StartCreateUserEvent;
        public event ErrorEventHandler ErrorCreateUserEvent;
        public event EventHandler EndCreateUserEvent;
        public event CancelEventHandler StartSynchronizeEvent;
        public event ErrorEventHandler ErrorSynchronizeEvent;
        public event EventHandler EndSynchronizeEvent;

        #endregion
        
        private string BaseUrl { get; set; }
        private HttpClient _httpClient;
        private List<string> _knownUsers = new List<string>();
        private string InternalFile => $"{BaseDirectory}/cloudvault.json";

        public Func<UserBusiness, string> GetUserDirectoryFunc { get; set; } =
            user => Path.GetInvalidPathChars()
                .Aggregate(user.User.Email, (current, c) => 
                    current.Replace(c, '_'));

        public CoreManager(Func<UserBusiness, string> getUserDirectoryFunc = null, string baseDirectory = "CloudVault")
        {
            if (getUserDirectoryFunc != null)
                GetUserDirectoryFunc = getUserDirectoryFunc;
            BaseDirectory = baseDirectory;
        }

        #region CoreApi

        public IEnumerable<string> KnownUsers()
        {
            CheckState(IsInitialized);
            return _knownUsers;
        }

        public bool Init(string baseUrl)
        {
            return IsInitialized = this.ActionCancellable(
                StartInitEvent,
                EndInitEvent,
                ErrorInitEvent, () =>
                {
                    Directory.CreateDirectory(BaseDirectory);
                    if (File.Exists(InternalFile))
                        LoadInternal();
                    else
                        SaveInternal();
                    BaseUrl = baseUrl;
                    VaultManager = new VaultManager(this);
                    _httpClient = new HttpClient
                    {
                        BaseAddress = new Uri(BaseUrl)
                    };
                    UserService = new UserService(_httpClient);
                    UpdateService = new UpdateService(_httpClient);
                    Mapper = CloudVaultMapperProfile.GenerateDefaultMapper();
                    IsInitialized = true;
                }) == ResultRun.Success;
        }

        public bool LoadVaults()
        {
            return IsVaultsLoaded = this.ActionCancellable(
                StartLoadVaultsEvent,
                EndLoadVaultsEvent,
                ErrorLoadVaultsEvent, () =>
                {
                    CheckState(IsInitialized, IsLogged);
                    VaultManager.Init(GetUserDirectoryFunc(UserBusiness));
                    IsVaultsLoaded = true;
                }) == ResultRun.Success;
        }

        public async Task<bool> LoadMetaDatas()
        {
            return IsMetaDataLoaded = await this.ActionAsyncCancellable(
                StartLoadMetaDatasEvent,
                EndLoadMetaDatasEvent,
                ErrorLoadMetaDatasEvent, async delegate
                {
                    CheckState(IsInitialized, IsLogged, IsVaultsLoaded);
                    await VaultManager.LoadMetaDatas();
                    IsMetaDataLoaded = true;
                }) == ResultRun.Success;
        }

        public async Task<Exception> CheckLocalPassword(string password)
        {
            var cipherMsgBytes = File.ReadAllBytes(
                $"{BaseDirectory}/{GetUserDirectoryFunc(UserBusiness)}/cipherMsg.key");
            var (success, _, e) =
                await Aes256Gcm.FastDecryptWithPassword(
                    cipherMsgBytes, password);
                        
            return success ? null : e;
        }
        public async Task<bool> Login(UserBusiness userBusiness)
        {
            return IsLogged = await this.ActionAsyncCancellable(
                StartLoginEvent,
                EndLoginEvent,
                ErrorLoginEvent, async delegate
                {
                    CheckState(IsInitialized);
                    UserBusiness = userBusiness;
                    if (!Directory.Exists($"{BaseDirectory}/{GetUserDirectoryFunc(userBusiness)}"))
                    {
                        var cipherMsgBytes = await ConnectToServer();
                        
                        Directory.CreateDirectory($"{BaseDirectory}/{GetUserDirectoryFunc(userBusiness)}");
                        File.WriteAllBytes($"{BaseDirectory}/{GetUserDirectoryFunc(userBusiness)}/cipherMsg.key",
                            cipherMsgBytes);
                    }
                    else
                    {
                        var e = await CheckLocalPassword(await UserBusiness.MasterPassword.Get());
                        if (e != null)
                            throw e;
                    }

                    if (!_knownUsers.Contains(userBusiness.User.Email))
                    {
                        _knownUsers.Add(userBusiness.User.Email);
                        SaveInternal();
                    }

                    if (File.Exists(SettingsFilePath))
                        LoadUserSettings();
                    else
                        SaveUserSettings();
                    IsLogged = true;
                }) == ResultRun.Success;
        }

        private async Task<byte[]> ConnectToServer()
        {
            var askMsgResponse = await UserService.AskMsgUserAsync(new AskMsgBody
            {
                Email = UserBusiness.User.Email
            });

            var cipherMsgBytes = Convert.FromBase64String(askMsgResponse.CipherMsg);
            var (success, msg, e) =
                await Aes256Gcm.FastDecryptWithPassword(
                    cipherMsgBytes,
                    await UserBusiness.MasterPassword.Get());
                    
            if (!success)
                throw e;
                    
            var userLoginResponse = await UserService.LoginUserAsync(new UserLoginBody
            {
                Email = UserBusiness.User.Email,
                Msg = msg
            });
                    
            if (!userLoginResponse)
                throw new SecurityException();
            return cipherMsgBytes;
        }

        public async Task<bool> Connect()
        {
            return IsConnected = await this.ActionAsyncCancellable(
                StartConnectEvent,
                EndConnectEvent,
                ErrorConnectEvent,async delegate
                {
                    CheckState(IsInitialized, IsLogged);

                    await ConnectToServer();

                    IsConnected = true;
                }) == ResultRun.Success;
        }

        public async Task<bool> Disconnect()
        {
            return !(IsConnected = await this.ActionAsyncCancellable(
                StartDisconnectEvent,
                EndDisconnectEvent,
                ErrorDisconnectEvent, async delegate
                {
                    CheckState(IsInitialized, IsLogged, IsConnected);

                    if (!await UserService.LogoutUserAsync())
                        throw new TimeoutException();

                    IsConnected = false;
                }) != ResultRun.Success);
        }

        public bool Logout()
        {
            return !(IsLogged = this.ActionCancellable(
                StartLogoutEvent,
                EndLogoutEvent,
                ErrorLogoutEvent, () =>
                {
                    CheckState(IsInitialized, IsLogged, !IsConnected);
                    UserBusiness = null;
                    IsVaultsLoaded = false;
                    IsMetaDataLoaded = false;
                    IsLogged = false;
                }) != ResultRun.Success);
        }

        public async Task<bool> CreateUser(UserBusiness userBusiness)
        {
            return await this.ActionAsyncCancellable(
                StartCreateUserEvent,
                EndCreateUserEvent,
                ErrorCreateUserEvent, async delegate
                {
                    CheckState(IsInitialized);
                    var userCreateResponse = await UserService.CreateUserAsync(new UserCreateBody
                        {Email = userBusiness.User.Email});

                    var (success, bytes, e) = await Aes256Gcm.FastEncryptWithPassword(
                        userCreateResponse.Msg,
                        await userBusiness.MasterPassword.Get());
                    
                    if (!success)
                        throw e;
                    
                    var finalized = await UserService.FinalizeUserAsync(new UserFinalizeBody
                    {
                        Guid = userCreateResponse.Guid,
                        CipherMsg = Convert.ToBase64String(bytes)
                    });

                    if (!finalized)
                        throw new OperationCanceledException("The mail could not be sent");

                    Directory.CreateDirectory($"{BaseDirectory}/{GetUserDirectoryFunc(userBusiness)}");
                    File.WriteAllBytes($"{BaseDirectory}/{GetUserDirectoryFunc(userBusiness)}/cipherMsg.key",
                        bytes);
                }) == ResultRun.Success;
        }

        public async Task<bool> SynchronizeWithServer()
        {
            return await this.ActionAsyncCancellable(
                       StartSynchronizeEvent,
                       EndSynchronizeEvent,
                       ErrorSynchronizeEvent, async delegate
                       {
                           CheckState(IsInitialized, IsLogged, IsConnected, IsVaultsLoaded, IsMetaDataLoaded);

                           var bodies = new List<UpdateDataBody>();

                           var lastSync = UserBusiness.User.UserOptions.LastSync;

                           IEnumerable<KeyValuePair<Vault, MetaData>> metaDatas;

                           if (lastSync == null)
                               metaDatas = VaultManager.MetaDatas;
                           else
                               metaDatas = VaultManager.MetaDatas
                                   .Where(vaultMeta => DateTime.Compare(
                                                           vaultMeta.Value.LastUpdate, (DateTime)lastSync) >= 0);

                           foreach (var (vault, meta) in metaDatas
                               .Select(vaultMeta => (vaultMeta.Key, vaultMeta.Value)))
                           {
                               var cipherMeta = Convert.ToBase64String(await Cipher.SerializeAsync(
                                   meta, await UserBusiness.MasterPassword.Get()));
                               var cipherData = Convert.ToBase64String(
                                   File.ReadAllBytes(vault.DataPath(this)));

                               bodies.Add(new UpdateDataBody
                               {
                                   ServerId = vault.ServerId,
                                   LocalGuid = vault.LocalGuid,
                                   LastUpdate = meta.LastUpdate,
                                   MetaData = cipherMeta,
                                   Data = cipherData
                               });
                           }

                           foreach (var (vault, deleteTime) in VaultManager.DeletedVaults
                               .Select(vaultTime => (vaultTime.Key, vaultTime.Value)))
                           {
                               bodies.Add(new UpdateDataBody
                               {
                                   ServerId = vault.ServerId,
                                   LocalGuid = vault.LocalGuid,
                                   LastUpdate = deleteTime,
                                   MetaData = null,
                                   Data = null
                               });
                           }
                    
                           var updateResponse = await UpdateService.UpdateDataAsync(new UpdateBody
                           {
                               LastSync = UserBusiness.User.UserOptions.LastSync,
                               Datas = bodies
                           });
                           UserBusiness.User.UserOptions.LastSync = updateResponse.LastSync;

                           foreach (var res in updateResponse.Datas)
                           {
                               var localGuid = res.LocalGuid;
                               if (localGuid != null)
                               {
                                   //Finalize creation -> Update ServerID
                                   VaultManager.Vaults.First(v => v.LocalGuid == localGuid)
                                       .ServerId = res.ServerId;
                                   break;
                               }

                               if (res.MetaData == null)
                               {
                                   //Delete
                                   var vault = VaultManager.Vaults.FirstOrDefault(
                                       v => v.ServerId == res.ServerId);
                                   if (vault != null)
                                       VaultManager.Delete(vault, false);
                               }
                               else
                               {
                                   //Update or creation
                                   var vault = VaultManager.Vaults.FirstOrDefault(
                                       v => v.ServerId == res.ServerId);
                                   if (vault == null) //creation
                                   {
                                       var guid = Guid.NewGuid();
                                       while (VaultManager.Vaults
                                           .ToList().FindAll(v => v.LocalGuid == guid.ToString()).Count > 0)
                                           guid = Guid.NewGuid();
                                       vault = new Vault(guid.ToString(), res.ServerId);
                                       VaultManager.Vaults.Add(vault);
                                   }
                                   var data = Convert.FromBase64String(res.Data);
                                   var metaData = Convert.FromBase64String(res.MetaData);
                                       
                                   File.WriteAllBytes(vault.DataPath(this), data);
                                   File.WriteAllBytes(vault.MetaDataPath(this), metaData);
                               }
                           }
                           VaultManager.Save();
                           VaultManager.DeletedVaults.Clear();
                           VaultManager.SaveDeletedVaults();
                           SaveUserSettings();
                       }) == ResultRun.Success;
        }

        public HashSet<Vault> GetVaults()
        {
            CheckState(IsInitialized, IsLogged, IsVaultsLoaded, IsMetaDataLoaded);
            return VaultManager.Vaults;
        }

        public ConcurrentDictionary<Vault, MetaData> GetMetaDatas()
        {
            CheckState(IsInitialized, IsLogged, IsVaultsLoaded, IsMetaDataLoaded);
            return VaultManager.MetaDatas;
        }

        #endregion

        private static bool CheckState(params bool[] predicates)
        {
            if (!predicates.All(it => it))
                throw new InvalidCallException();
            return true;
        }

        public void SaveUserSettings() => File.WriteAllText(SettingsFilePath,
            CoreSerializer.Serialize(Mapper.Map<UserOptionsDto>(UserBusiness.User.UserOptions)));

        private void LoadUserSettings() => UserBusiness.User.UserOptions = Mapper.Map<UserOptions>(
            CoreSerializer.Deserialize<UserOptionsDto>(File.ReadAllText(SettingsFilePath)));

        #region Internal

        private void SaveInternal()
        {
            File.WriteAllText(InternalFile, CoreSerializer.Serialize(
                    new CoreInternalDto {KnownUsers = _knownUsers}));
        }

        private void LoadInternal()
        {
            _knownUsers = CoreSerializer.Deserialize<CoreInternalDto>(
                File.ReadAllText(InternalFile)).KnownUsers;
        }

        #endregion
        
    }
}
