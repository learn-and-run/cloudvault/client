﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using AutoMapper;
using CloudVault_ClientCore.BusinessManagement.service;
using CloudVault_ClientCore.BusinessManagement.user;
using CloudVault_ClientCore.BusinessManagement.utils;
using CloudVault_ClientCore.BusinessManagement.vault;
using CloudVault_ClientCore.Dbo.metadata;
using CloudVault_ClientCore.Dbo.vault;

namespace CloudVault_ClientCore.BusinessManagement.manager
{
    public interface ICoreManager
    {
        string BaseDirectory { get; }
        IMapper Mapper { get; }
        UserBusiness UserBusiness { get; }
        VaultManager VaultManager { get; }
        UserService UserService { get; }
        UpdateService UpdateService { get; }
        
        bool IsInitialized { get; set; }
        bool IsVaultsLoaded { get; set; }
        bool IsMetaDataLoaded { get; set; }
        bool IsLogged { get; set; }
        bool IsConnected { get; set; }

        event CancelEventHandler StartInitEvent;
        event ErrorEventHandler ErrorInitEvent;
        event EventHandler EndInitEvent;
        
        event CancelEventHandler StartLoadVaultsEvent;
        event ErrorEventHandler ErrorLoadVaultsEvent;
        event EventHandler EndLoadVaultsEvent;
        
        event CancelEventHandler StartLoadMetaDatasEvent;
        event ErrorEventHandler ErrorLoadMetaDatasEvent;
        event EventHandler EndLoadMetaDatasEvent;
        
        event CancelEventHandler StartLoginEvent;
        event ErrorEventHandler ErrorLoginEvent;
        event EventHandler EndLoginEvent;
        
        event CancelEventHandler StartConnectEvent;
        event ErrorEventHandler ErrorConnectEvent;
        event EventHandler EndConnectEvent;
        
        event CancelEventHandler StartDisconnectEvent;
        event ErrorEventHandler ErrorDisconnectEvent;
        event EventHandler EndDisconnectEvent;
        
        event CancelEventHandler StartLogoutEvent;
        event ErrorEventHandler ErrorLogoutEvent;
        event EventHandler EndLogoutEvent;
        
        event CancelEventHandler StartCreateUserEvent;
        event ErrorEventHandler ErrorCreateUserEvent;
        event EventHandler EndCreateUserEvent;

        event CancelEventHandler StartSynchronizeEvent;
        event ErrorEventHandler ErrorSynchronizeEvent;
        event EventHandler EndSynchronizeEvent;

        /// <summary>
        /// Function that compute the user directory name/path
        /// </summary>
        Func<UserBusiness, string> GetUserDirectoryFunc { get; set; }

        
        /// <summary>
        /// Function that lists known users
        /// </summary>
        IEnumerable<string> KnownUsers();
        
        /// <summary>
        /// Init the core manager
        /// </summary>
        bool Init(string baseUrl);
        
        /// <summary>
        /// Load Vaults (without metadatas) (must be logged)
        /// </summary>
        bool LoadVaults();

        /// <summary>
        /// Load Metadatas (vaults must be loaded)
        /// </summary>
        Task<bool> LoadMetaDatas();

        /// <summary>
        /// Log in the user in the app (does not connect *see Connect()*) (must be initialized)
        /// </summary>
        Task<bool> Login(UserBusiness userBusiness);

        /// <summary>
        /// If logged in, connect the user to the server (must be logged)
        /// </summary>
        Task<bool> Connect();

        /// <summary>
        /// Disconnect from the server (does not log out *see Logout()*) (must be connected)
        /// </summary>
        Task<bool> Disconnect();

        /// <summary>
        /// If disconnected, log out the user (must be logged)
        /// </summary>
        bool Logout();

        /// <summary>
        /// If not logged in, create a user
        /// (must be initialized) (must be online)
        /// </summary>
        Task<bool> CreateUser(UserBusiness userBusiness);

        /// <summary>
        /// Synchronize local datas with server datas
        /// (must be logged, connected, vaults loaded, metadatas loaded)
        /// </summary>
        Task<bool> SynchronizeWithServer();
        
        /// <summary>
        /// Get vaults (must be logged, vaults loaded, metadatas loaded)
        /// </summary>
        HashSet<Vault> GetVaults();
        
        /// <summary>
        /// Get MetaDatas (must be logged, vaults loaded, metadatas loaded)
        /// </summary>
        ConcurrentDictionary<Vault, MetaData> GetMetaDatas();

    }
}
