﻿using System;
using System.Collections.Generic;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Security;

namespace CloudVault_ClientCore.BusinessManagement.security
{
    /// <summary>
    /// We use PBKDF2_SHA256 instead of PBKDF2_HMACSHA1 because SHA1 is broken
    /// In fact, SHA1 is broken but not PBKDF2_HMACSHA1 BUT to be totally safe and durable :
    /// I prefer to use PBKDF2_SHA256
    /// Uses NuGet Package BouncyCastle.Crypto.dll
    /// Part of that code is from
    /// https://stackoverflow.com/questions/34950611/how-to-create-a-pbkdf2-sha256-password-hash-in-c-sharp-bouncy-castle/34990110
    /// To better understand the slow equals (avoid timing attacks)
    /// https://stackoverflow.com/questions/16953231/cryptography-net-avoiding-timing-attack
    /// </summary>
    public static class Pbkdf2Sha256
    {
        private static readonly SecureRandom Random = new SecureRandom();

        //Preconfigured Password Key Derivation Parameters
        public static readonly int SaltBitSize = 256;
        public static readonly int Iterations = 200000;
        public static readonly int HashByteSize = 32;
        public static readonly int MinPasswordLength = 10;

        public static byte[] GetKeyFromPassword(string password, out byte[] salt)
        {
            if (password.Length < MinPasswordLength)
                throw new ArgumentException($"The password size must be {MinPasswordLength} or longer");
            salt = CreateSalt(SaltBitSize / 8);
            return Pbkdf2Sha256_GetHash(password, salt, Iterations, HashByteSize);
        }

        public static byte[] GetKeyFromPasswordAndSalt(string password, byte[] salt)
        {
            return Pbkdf2Sha256_GetHash(password, salt, Iterations, HashByteSize);
        }

        /// <summary>
        /// Random Salt Creation
        /// </summary>
        /// <param name="size">The size of the salt in bytes</param>
        /// <returns>A random salt of the required size.</returns>
        public static byte[] CreateSalt(int size)
        {
            var salt = new byte[size];
            Random.NextBytes(salt);
            return salt;
        }

        /// <summary>
        /// Gets a PBKDF2_SHA256 Hash  (Overload)
        /// </summary>
        /// <param name="password">The password as a plain text string</param>
        /// <param name="saltAsBase64String">The salt for the password</param>
        /// <param name="iterations">The number of times to encrypt the password</param>
        /// <param name="hashByteSize">The byte size of the final hash</param>
        /// <returns>A base64 string of the hash.</returns>
        public static string Pbkdf2Sha256_GetHash(string password, string saltAsBase64String, int iterations, int hashByteSize)
        {
            var saltBytes = Convert.FromBase64String(saltAsBase64String);
            var hash = Pbkdf2Sha256_GetHash(password, saltBytes, iterations, hashByteSize);
            return Convert.ToBase64String(hash);
        }

        /// <summary>
        /// Gets a PBKDF2_SHA256 Hash (CORE METHOD)
        /// </summary>
        /// <param name="password">The password as a plain text string</param>
        /// <param name="salt">The salt as a byte array</param>
        /// <param name="iterations">The number of times to encrypt the password</param>
        /// <param name="hashByteSize">The byte size of the final hash</param>
        /// <returns>A the hash as a byte array.</returns>
        public static byte[] Pbkdf2Sha256_GetHash(string password, byte[] salt, int iterations, int hashByteSize)
        {
            var pdb = new Pkcs5S2ParametersGenerator(new Org.BouncyCastle.Crypto.Digests.Sha256Digest());
            pdb.Init(PbeParametersGenerator.Pkcs5PasswordToBytes(password.ToCharArray()), salt, iterations);
            var key = (KeyParameter)pdb.GenerateDerivedMacParameters(hashByteSize * 8);
            return key.GetKey();
        }

        /// <summary>
        /// Validates a password given a hash of the correct one. (OVERLOAD)
        /// </summary>
        /// <param name="password">The original password to hash</param>
        /// <param name="salt">The salt that was used when hashing the password</param>
        /// <param name="iterations">The number of times it was encrypted</param>
        /// <param name="hashByteSize">The byte size of the final hash</param>
        /// <param name="hashAsBase64String">The hash the password previously provided as a base64 string</param>
        /// <returns>True if the hashes match</returns>
        public static bool ValidatePassword(string password, string salt, int iterations,
            int hashByteSize, string hashAsBase64String)
        {
            var saltBytes = Convert.FromBase64String(salt);
            var actualHashBytes = Convert.FromBase64String(hashAsBase64String);
            return ValidatePassword(password, saltBytes, iterations, hashByteSize, actualHashBytes);
        }

        /// <summary>
        /// Validates a password given a hash of the correct one (MAIN METHOD).
        /// </summary>
        /// <param name="password">The password to check.</param>
        /// <param name="saltBytes">The salt</param>
        /// <param name="iterations">Number of iterations</param>
        /// <param name="hashByteSize">The byte size of the final hash</param>
        /// <param name="actualGainedHasAsByteArray"></param>
        /// <returns>True if the password is correct. False otherwise.</returns>
        public static bool ValidatePassword(string password, byte[] saltBytes, int iterations,
            int hashByteSize, byte[] actualGainedHasAsByteArray)
        {
            var testHash = Pbkdf2Sha256_GetHash(password, saltBytes, iterations, hashByteSize);
            return SlowEquals(actualGainedHasAsByteArray, testHash);
        }

        /// <summary>
        /// Compares two byte arrays in length-constant time. This comparison
        /// method is used so that password hashes cannot be extracted from
        /// on-line systems using a timing attack and then attacked off-line.
        /// </summary>
        /// <param name="a">The first byte array.</param>
        /// <param name="b">The second byte array.</param>
        /// <returns>True if both byte arrays are equal. False otherwise.</returns>
        private static bool SlowEquals(IReadOnlyList<byte> a, IReadOnlyList<byte> b)
        {
            var diff = (uint)a.Count ^ (uint)b.Count;
            for (var i = 0; i < a.Count && i < b.Count; i++)
                diff |= (uint)(a[i] ^ b[i]);
            return diff == 0;
        }

    }
}
