﻿using System.IO;
using System.Threading.Tasks;
using CloudVault_ClientCore.BusinessManagement.utils;

namespace CloudVault_ClientCore.BusinessManagement.security
{
    public static class Cipher
    {
        public static async Task<byte[]> SerializeAsync<T>(T t, string masterPassword)
        {
            var (success, cipherText, e) =
                await Aes256Gcm.FastEncryptWithPassword(
                    CoreSerializer.Serialize(t), masterPassword);
            if (!success) throw e;
            return cipherText;
        }

        public static async Task<T> DeserializeAsync<T>(byte[] cipherBytes, string masterPassword)
        {
            var (success, plainText, e) =
                await Aes256Gcm.FastDecryptWithPassword(cipherBytes, masterPassword);
            if (!success) throw e;
            return CoreSerializer.Deserialize<T>(plainText);
        }
        
        public static async Task SaveAsync<T>(T t, string filePath, string masterPassword)
        {
            File.WriteAllBytes(filePath, await SerializeAsync(t, masterPassword));
        }

        public static async Task<T> LoadAsync<T>(string filePath, string masterPassword)
        {
            return await DeserializeAsync<T>(File.ReadAllBytes(filePath), masterPassword);
        }

    }
}
