﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Modes;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Security;

namespace CloudVault_ClientCore.BusinessManagement.security
{
    public static class Aes256Gcm
    {
        private static readonly SecureRandom Random = new SecureRandom();

        //Preconfigured Encryption Parameters
        public const int NonceBitSize = 128;
        public const int MacBitSize = 128;
        public const int KeyBitSize = 256;

        public static async Task<(bool, byte[], Exception)> FastEncryptWithPassword(
            string plainText,
            string password,
            byte[] nonSecretPayload = null)
        {
            using (var plainTextInStream = new MemoryStream(Encoding.UTF8.GetBytes(plainText)))
            {
                using (var cipherTextOutStream = new MemoryStream(plainText.Length))
                {
                    var (success, e) = await EncryptWithPasswordStreamAsync(
                        plainTextInStream, cipherTextOutStream, password, nonSecretPayload);
                    return (success, cipherTextOutStream.ToArray(), e);
                }
            }
        }

        public static async Task<(bool, string, Exception)> FastDecryptWithPassword(
            byte[] cipherText,
            string password,
            int nonSecretPayloadLength = 0)
        {
            using (var cipherTextInStream = new MemoryStream(cipherText))
            {
                using (var plainTextOutStream = new MemoryStream(cipherText.Length))
                {
                    var (success, e) = await DecryptWithPasswordStreamAsync(
                        cipherTextInStream, plainTextOutStream, password, nonSecretPayloadLength);
                    var plainText = Encoding.UTF8.GetString(plainTextOutStream.ToArray());
                    return (success, plainText, e);
                }
            }
        }

        public static async Task<(bool, byte[], Exception)> FastEncrypt(
            string plainText,
            byte[] key,
            byte[] nonSecretPayload = null)
        {
            using (var plainTextInStream = new MemoryStream(Encoding.UTF8.GetBytes(plainText)))
            {
                using (var cipherTextOutStream = new MemoryStream(plainText.Length))
                {
                    var (success, e) = await EncryptStreamAsync(
                        plainTextInStream, cipherTextOutStream, key, nonSecretPayload);
                    return (success, cipherTextOutStream.ToArray(), e);
                }
            }
        }

        public static async Task<(bool, string, Exception)> FastDecrypt(
            byte[] cipherText,
            byte[] key,
            int nonSecretPayloadLength = 0)
        {
            using (var cipherTextInStream = new MemoryStream(cipherText))
            {
                using (var plainTextOutStream = new MemoryStream(cipherText.Length))
                {
                    var (success, e) = await DecryptStreamAsync(
                        cipherTextInStream, plainTextOutStream, key, nonSecretPayloadLength);
                    var plainText = Encoding.UTF8.GetString(plainTextOutStream.ToArray());
                    return (success, plainText, e);
                }
            }
        }

        public static async Task<(bool, Exception)> EncryptWithPasswordStreamAsync(
            Stream plainTextInStream,
            Stream cipherTextOutStream,
            string password,
            byte[] nonSecretPayload = null)
        {
            try
            {
                //Derive key from password
                var key = Pbkdf2Sha256.GetKeyFromPassword(password, out var salt);
                //Append Payload
                nonSecretPayload = nonSecretPayload ?? new byte[] { };
                await WriteAsync(cipherTextOutStream, nonSecretPayload);
                //Append Nonce
                var nonce = new byte[NonceBitSize / 8];
                Random.NextBytes(nonce, 0, nonce.Length);
                await WriteAsync(cipherTextOutStream, nonce);
                //Append Salt
                await WriteAsync(cipherTextOutStream, salt);
                //Encrypt
                var cipher = GenerateCipher(true, key, nonce, nonSecretPayload);
                return await ApplyCipherToStream(cipher, plainTextInStream, cipherTextOutStream);
            } catch (ArgumentException e)
            {
                return (false, e);
            }
        }

        public static async Task<(bool, Exception)> DecryptWithPasswordStreamAsync(
            Stream cipherTextInStream,
            Stream plainTextOutStream,
            string password,
            int nonSecretPayloadLength = 0)
        {
            try
            {
                //Grab Payload
                var nonSecretPayload = await GrabAsync(cipherTextInStream, nonSecretPayloadLength);
                //Grab Nonce
                var nonce = await GrabAsync(cipherTextInStream, NonceBitSize / 8);
                //Grab Salt
                var salt = await GrabAsync(cipherTextInStream, Pbkdf2Sha256.SaltBitSize / 8);
                //Regenerate from password and salt
                var key = Pbkdf2Sha256.GetKeyFromPasswordAndSalt(password, salt);
                //Decrypt
                var cipher = GenerateCipher(false, key, nonce, nonSecretPayload);
                return await ApplyCipherToStream(cipher, cipherTextInStream, plainTextOutStream);
            } catch (ArgumentException e)
            {
                return (false, e);
            }
        }

        public static async Task<(bool, Exception)> EncryptStreamAsync(
            Stream plainTextInStream,
            Stream cipherTextOutStream,
            byte[] key,
            byte[] nonSecretPayload = null)
        {
            try
            {
                //Append Payload
                nonSecretPayload = nonSecretPayload ?? new byte[] { };
                await WriteAsync(cipherTextOutStream, nonSecretPayload);
                //Append Nonce
                var nonce = new byte[NonceBitSize / 8];
                Random.NextBytes(nonce, 0, nonce.Length);
                await WriteAsync(cipherTextOutStream, nonce);
                //Encrypt
                var cipher = GenerateCipher(true, key, nonce, nonSecretPayload);
                return await ApplyCipherToStream(cipher, plainTextInStream, cipherTextOutStream);
            } catch (ArgumentException e)
            {
                return (false, e);
            }
        }


        public static async Task<(bool, Exception)> DecryptStreamAsync(
            Stream cipherTextInStream,
            Stream plainTextOutStream,
            byte[] key,
            int nonSecretPayloadLength = 0)
        {
            try
            {
                //Grab Payload
                var nonSecretPayload = await GrabAsync(cipherTextInStream, nonSecretPayloadLength);
                //Grab Nonce
                var nonce = await GrabAsync(cipherTextInStream, NonceBitSize / 8);
                //Decrypt
                var cipher = GenerateCipher(false, key, nonce, nonSecretPayload);
                return await ApplyCipherToStream(cipher, cipherTextInStream, plainTextOutStream);
            } catch (ArgumentException e)
            {
                return (false, e);
            }
        }

        private static async Task<(bool, InvalidCipherTextException)> ApplyCipherToStream(
            IAeadBlockCipher cipher,
            Stream inStream,
            Stream outStream)
        {
            CheckInStream(inStream);
            CheckOutStream(outStream);
            try
            {
                var input = new byte[8192];
                var output = new byte[cipher.GetBlockSize() + cipher.GetOutputSize(input.Length)];
                int bytesRead;
                while ((bytesRead = await inStream.ReadAsync(input, 0, input.Length)) > 0)
                {
                    var count = cipher.ProcessBytes(input, 0, bytesRead, output, 0);
                    await outStream.WriteAsync(output, 0, count);
                }
                var lastSize = cipher.DoFinal(output, 0);
                await outStream.WriteAsync(output, 0, lastSize);
            } catch (InvalidCipherTextException e)
            {
                return (false, e);
            }
            return (true, null);
        }

        private static void CheckKey(byte[] key)
        {
            if (key == null || key.Length != KeyBitSize / 8)
                throw new ArgumentException($"Key needs to be {KeyBitSize} bit!", nameof(key));
        }
        private static void CheckNonce(byte[] nonce)
        {
            if (nonce == null || nonce.Length != NonceBitSize / 8)
                throw new ArgumentException($"Nonce size must be {NonceBitSize}", nameof(nonce));
        }

        private static void CheckInStream(Stream inStream)
        {
            if (inStream == null || !inStream.CanRead)
                throw new ArgumentException("The plainTextInStream and cipherTextOutStream must not be null!", nameof(inStream));
        }
        private static void CheckOutStream(Stream outStream)
        {
            if (outStream == null || !outStream.CanWrite)
                throw new ArgumentException("The plainTextInStream and cipherTextOutStream must not be null!", nameof(outStream));
        }

        private static async Task<byte[]> GrabAsync(Stream inStream, int size)
        {
            //Check input stream
            CheckInStream(inStream);

            var bytes = new byte[size];
            if (await inStream.ReadAsync(bytes, 0, size) != size)
                throw new ArgumentException("The input stream does not contain enough cipherBytes to read", nameof(inStream));
            return bytes;
        }

        private static async Task WriteAsync(Stream outStream, byte[] bytesToWrite)
        {
            //Check output stream
            CheckOutStream(outStream);

            await outStream.WriteAsync(bytesToWrite, 0, bytesToWrite.Length);
        }

        private static IAeadBlockCipher GenerateCipher(bool forEncryption, byte[] key, byte[] nonce, byte[] nonSecretPayload)
        {
            CheckKey(key);
            CheckNonce(nonce);

            var cipher = new GcmBlockCipher(new AesEngine());
            var parameters = new AeadParameters(new KeyParameter(key), MacBitSize, nonce, nonSecretPayload);
            cipher.Init(forEncryption, parameters);
            return cipher;
        }

    }
}
