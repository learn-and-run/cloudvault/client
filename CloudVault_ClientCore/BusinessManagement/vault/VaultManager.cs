﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudVault_ClientCore.BusinessManagement.manager;
using CloudVault_ClientCore.BusinessManagement.metadata;
using CloudVault_ClientCore.BusinessManagement.security;
using CloudVault_ClientCore.BusinessManagement.utils;
using CloudVault_ClientCore.BusinessManagement.vault.events;
using CloudVault_ClientCore.Dbo.data;
using CloudVault_ClientCore.Dbo.metadata;
using CloudVault_ClientCore.Dbo.vault;
using CloudVault_ClientCore.Dto.metadata;
using CloudVault_ClientCore.Dto.vault;
using Newtonsoft.Json;

namespace CloudVault_ClientCore.BusinessManagement.vault
{
    public class VaultManager
    {
        #region Properties

        private readonly ICoreManager _coreManager;
        public string VaultsDirectoryPath { get; private set; }
        public string VaultFilePath => $"{VaultsDirectoryPath}/vaults.json";
        public string DeletedVaultFilePath => $"{VaultsDirectoryPath}/deleted.json";
        public HashSet<Vault> Vaults { get; private set; }
        public Dictionary<Vault, DateTime> DeletedVaults { get; private set; }
        public ConcurrentDictionary<Vault, MetaData> MetaDatas { get; private set; }

        #endregion

        #region Events

        public delegate void MetaDataLoadStartEventHandler(object sender, MetaDataLoadStartEventArgs e);
        public delegate void MetaDataLoadEndEventHandler(object sender, MetaDataLoadEndEventArgs e);
        public delegate void MetaDataLoadingIndexEventHandler(object sender, MetaDataLoadingIndexEventArgs e);
        public event MetaDataLoadStartEventHandler MetaDataLoadStartEvent;
        public event MetaDataLoadEndEventHandler MetaDataLoadEndEvent;
        public event MetaDataLoadingIndexEventHandler MetaDataLoadingIndexEvent;

        public delegate void DataModifiedEventHandler(object sender, DataModifiedEventArgs e);
        public delegate void DataAddedEventHandler(object sender, DataAddedEventArgs e);
        public delegate void DataDeletedEventHandler(object sender, DataDeletedEventArgs e);
        
        public event DataModifiedEventHandler DataModifiedEvent;
        public event DataAddedEventHandler DataAddedEvent;
        public event DataDeletedEventHandler DataDeletedEvent;

        #endregion

        public VaultManager(ICoreManager coreManager)
        {
            _coreManager = coreManager;
        }

        public void Init(string userDirectoryPath)
        {
            MetaDatas = new ConcurrentDictionary<Vault, MetaData>();
            DeletedVaults = new Dictionary<Vault, DateTime>();
            VaultsDirectoryPath = $"{_coreManager.BaseDirectory}/{userDirectoryPath}/vaults";
            Directory.CreateDirectory(VaultsDirectoryPath);
            Directory.CreateDirectory(VaultsDirectoryPath + "/data");
            Directory.CreateDirectory(VaultsDirectoryPath + "/metadata");
            if (File.Exists(VaultFilePath))
                Load();
            else
                Save();
            if (File.Exists(DeletedVaultFilePath))
                LoadDeletedVaults();
            else
                SaveDeletedVaults();
        }

        #region VaultLoadSave
        public void Save()
        {
            var list = (Vaults ?? (Vaults = new HashSet<Vault>()))
                .Select(vault => _coreManager.Mapper.Map<VaultDto>(vault))
                .ToList();
            var json = CoreSerializer.Serialize(list);
            File.WriteAllText(VaultFilePath, json, Encoding.UTF8);
        }

        public void Load()
        {
            var json = File.ReadAllText(VaultFilePath, Encoding.UTF8);
            var list = CoreSerializer.Deserialize<List<VaultDto>>(json);
            Vaults = list
                .Select(vaultDto => _coreManager.Mapper.Map<Vault>(vaultDto))
                .ToHashSet();
        }
        #endregion
        
        #region VaultLoadSave
        public void SaveDeletedVaults()
        {
            File.WriteAllText(DeletedVaultFilePath,
                JsonConvert.SerializeObject(DeletedVaults), Encoding.UTF8);
        }

        public void LoadDeletedVaults()
        {
            DeletedVaults = JsonConvert.DeserializeObject<Dictionary<Vault, DateTime>>(
                File.ReadAllText(DeletedVaultFilePath, Encoding.UTF8));
        }
        #endregion

        public async Task LoadMetaDatas()
        {
            MetaDatas.Clear();
            var password = await _coreManager.UserBusiness.MasterPassword.Get();
            MetaDataLoadStartEvent?.Invoke(this, new MetaDataLoadStartEventArgs(Vaults.Count));
            var state = 0;

            Task.WaitAll(Vaults.Select(vault => Task.Run(async delegate
            {
                var metaData = await vault.GetMetaData(_coreManager, password);
                MetaDatas.TryAdd(vault, metaData);
                MetaDataLoadingIndexEvent?.Invoke(this, new MetaDataLoadingIndexEventArgs(state, Vaults.Count, vault, metaData));
                state++;
            })).ToArray());

            MetaDataLoadEndEvent?.Invoke(this, new MetaDataLoadEndEventArgs(Vaults.Count));
        }

        #region ManageData
        public async Task<(Vault, MetaData)> AddData(Data data, string masterPassword)
        {
            var metaData = new MetaData(data);
            var guid = Guid.NewGuid();
            while (Vaults.ToList().FindAll(v => v.LocalGuid == guid.ToString()).Count > 0)
                guid = Guid.NewGuid();
            var vault = new Vault(guid);
            Vaults.Add(vault);
            var metaDataDto = _coreManager.Mapper.Map<MetaDataDto>(metaData);
            await Cipher.SaveAsync(metaDataDto, vault.MetaDataPath(_coreManager), masterPassword);
            await Cipher.SaveAsync(data, vault.DataPath(_coreManager), masterPassword);
            Save();
            MetaDatas.TryAdd(vault, metaData);
            DataAddedEvent?.Invoke(this, new DataAddedEventArgs(vault, metaData));
            return (vault, metaData);
        }


        public (Vault, MetaData) Delete(Vault vault, bool saveDeletion = true)
        {
            Vaults.Remove(vault);
            File.Delete(vault.MetaDataPath(_coreManager));
            File.Delete(vault.DataPath(_coreManager));
            var metaData = MetaDatas[vault];
            MetaDatas.TryRemove(vault, out _);
            DataDeletedEvent?.Invoke(this, new DataDeletedEventArgs(vault, metaData));
            if (saveDeletion)
            {
                Save();
                if (vault.ServerId != 0)
                {
                    DeletedVaults.Add(vault, DateTime.Now);
                    SaveDeletedVaults();
                }
            }
            return (vault, metaData);
        }

        public async Task<MetaData> Modify(Vault vault, Data data, MetaData metaData, string masterPassword)
        {
            var oldMetaData = metaData.Clone() as MetaData;
            metaData.UpdateFrom(data);
            var metaDataDto = _coreManager.Mapper.Map<MetaDataDto>(metaData);
            await Cipher.SaveAsync(metaDataDto, vault.MetaDataPath(_coreManager), masterPassword);
            await Cipher.SaveAsync(data, vault.DataPath(_coreManager), masterPassword);
            DataModifiedEvent?.Invoke(this,
                new DataModifiedEventArgs(vault, metaData, oldMetaData));
            Save();
            return oldMetaData;
        }
        #endregion
    }
}
