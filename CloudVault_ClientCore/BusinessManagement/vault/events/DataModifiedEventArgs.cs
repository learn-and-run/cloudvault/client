﻿using CloudVault_ClientCore.Dbo.metadata;
using CloudVault_ClientCore.Dbo.vault;

namespace CloudVault_ClientCore.BusinessManagement.vault.events
{
    public class DataModifiedEventArgs
    {
        public Vault VaultUpdated { get; set; }
        public MetaData OldMetaData { get; set; }
        public MetaData NewMetaData { get; set; }

        public DataModifiedEventArgs(Vault vault, MetaData newMetaData, MetaData oldMetaData)
        {
            VaultUpdated = vault;
            NewMetaData = newMetaData;
            OldMetaData = oldMetaData;
        }
    }
}
