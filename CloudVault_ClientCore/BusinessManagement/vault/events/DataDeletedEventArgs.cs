﻿using CloudVault_ClientCore.Dbo.metadata;
using CloudVault_ClientCore.Dbo.vault;

namespace CloudVault_ClientCore.BusinessManagement.vault.events
{
    public class DataDeletedEventArgs
    {
        public Vault VaultDeleted { get; set; }
        public MetaData MetaDataDeleted { get; set; }
        public DataDeletedEventArgs(Vault vault, MetaData metaData)
        {
            VaultDeleted = vault;
            MetaDataDeleted = metaData;
        }
    }
}
