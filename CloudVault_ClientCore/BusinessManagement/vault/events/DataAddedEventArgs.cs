﻿using CloudVault_ClientCore.Dbo.metadata;
using CloudVault_ClientCore.Dbo.vault;

namespace CloudVault_ClientCore.BusinessManagement.vault.events
{
    public class DataAddedEventArgs
    {
        public Vault VaultAdded { get; set; }
        public MetaData MetaDataAdded { get; set; }
        public DataAddedEventArgs(Vault vault, MetaData metaData)
        {
            VaultAdded = vault;
            MetaDataAdded = metaData;
        }
    }
}
