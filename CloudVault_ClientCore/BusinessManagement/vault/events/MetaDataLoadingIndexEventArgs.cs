﻿using CloudVault_ClientCore.Dbo.metadata;
using CloudVault_ClientCore.Dbo.vault;

namespace CloudVault_ClientCore.BusinessManagement.vault.events
{
    public class MetaDataLoadingIndexEventArgs
    {
        public int Index { get; set; }
        public int Total { get; set; }
        public Vault VaultLoaded { get; set; }
        public MetaData MetaDataLoaded { get; set; }

        public MetaDataLoadingIndexEventArgs(int index, int total, Vault vault, MetaData metaData)
        {
            Index = index;
            Total = total;
            VaultLoaded = vault;
            MetaDataLoaded = metaData;
        }
    }
}
