﻿namespace CloudVault_ClientCore.BusinessManagement.vault.events
{
    public class MetaDataLoadEndEventArgs
    {
        public int Total { get; set; }

        public MetaDataLoadEndEventArgs(int total)
        {
            Total = total;
        }
    }
}
