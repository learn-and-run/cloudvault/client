﻿namespace CloudVault_ClientCore.BusinessManagement.vault.events
{
    public class MetaDataLoadStartEventArgs
    {
        public int Total { get; set; }
        
        public MetaDataLoadStartEventArgs(int total)
        {
            Total = total;
        }

    }
}
