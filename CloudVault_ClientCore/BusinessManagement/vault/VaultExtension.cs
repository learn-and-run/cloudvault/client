﻿using System.Threading.Tasks;
using CloudVault_ClientCore.BusinessManagement.manager;
using CloudVault_ClientCore.BusinessManagement.security;
using CloudVault_ClientCore.Dbo.data;
using CloudVault_ClientCore.Dbo.data.exception;
using CloudVault_ClientCore.Dbo.metadata;
using CloudVault_ClientCore.Dbo.vault;
using CloudVault_ClientCore.Dto.metadata;

namespace CloudVault_ClientCore.BusinessManagement.vault
{
    public static class VaultExtension
    {
        public static string DataPath(this Vault vault, ICoreManager coreManager) =>
            $"{coreManager.VaultManager.VaultsDirectoryPath}/data/{vault.LocalGuid}.cvdata";

        public static string MetaDataPath(this Vault vault, ICoreManager coreManager) =>
            $"{coreManager.VaultManager.VaultsDirectoryPath}/metadata/{vault.LocalGuid}.cvmeta";

        public static async Task<MetaData> GetMetaData(this Vault vault,
            ICoreManager coreManager, string masterPassword)
        {
            var metaDataDto = await Cipher.LoadAsync<MetaDataDto>(vault.MetaDataPath(coreManager), masterPassword);
            return coreManager.Mapper.Map<MetaData>(metaDataDto);
        }

        public static async Task<(Data, MetaData)> GetDataAndMetaData(this Vault vault,
            ICoreManager coreManager, string masterPassword)
        {
            var metaData = await vault.GetMetaData(coreManager, masterPassword);
            return (await vault.GetData(coreManager, masterPassword, metaData.DataType), metaData);
        }

        public static async Task<Data> GetData(this Vault vault,
            ICoreManager coreManager, string masterPassword, DataType dataType)
        {
            switch (dataType)
            {
                case DataType.Password:
                    return await Cipher.LoadAsync<PasswordData>(vault.DataPath(coreManager), masterPassword);
                case DataType.Note:
                    return await Cipher.LoadAsync<NoteData>(vault.DataPath(coreManager), masterPassword);
                case DataType.CreditCard:
                    return await Cipher.LoadAsync<CreditCardData>(vault.DataPath(coreManager), masterPassword);
                default:
                    throw new DataTypeUnknownException();
            }
        }
    }
}
