﻿using System.Threading.Tasks;

namespace CloudVault_ClientCore.BusinessManagement.user
{
    public interface IPasswordAsker
    {
        Task<string> AskPassword();
    }
}
