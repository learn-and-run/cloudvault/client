﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CloudVault_ClientCore.Dbo.user;

namespace CloudVault_ClientCore.BusinessManagement.user
{
    public class MasterPassword
    {
        private string _masterPassword;
        private readonly IUser _user;
        private readonly IPasswordAsker _asker;
        private CancellationTokenSource _source;

        private int DelayForgot => _user.UserOptions.DelayForgotPasswordInSecond * 1000;

        public MasterPassword(IUser user, string masterPassword, IPasswordAsker asker)
        {
            _user = user;
            _masterPassword = masterPassword;
            _asker = asker;
            RestartForgotTask();
        }

        public void RestartForgotTask()
        {
            _source?.Cancel();
            var delay = DelayForgot;
            if (delay <= 0)
                return;
            Task.Run(async delegate
            {
                _source = new CancellationTokenSource();
                await Task.Delay(Math.Max(delay, 0) + 1000, _source.Token);//add 1 second
                _masterPassword = null;
                _source?.Dispose();
                _source = null;
            });
        }

        public async Task<string> Get()
        {
            _source?.Cancel();
            _masterPassword = _masterPassword ?? await _asker.AskPassword();
            RestartForgotTask();
            return _masterPassword;
        }
    }
}
