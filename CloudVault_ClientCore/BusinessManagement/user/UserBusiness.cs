﻿using CloudVault_ClientCore.Dbo.user;

namespace CloudVault_ClientCore.BusinessManagement.user
{
    public class UserBusiness
    {
        public IUser User { get; }
        public MasterPassword MasterPassword { get; }

        public UserBusiness(IUser user, IPasswordAsker passwordAsker, string masterPassword)
        {
            User = user;
            MasterPassword = new MasterPassword(user, masterPassword, passwordAsker);
        }
    }
}
