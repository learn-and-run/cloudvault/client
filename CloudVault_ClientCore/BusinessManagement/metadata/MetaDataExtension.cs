﻿using System;
using CloudVault_ClientCore.Dbo.data;
using CloudVault_ClientCore.Dbo.metadata;

namespace CloudVault_ClientCore.BusinessManagement.metadata
{
    public static class MetaDataExtension
    {
        public static void UpdateFrom(this MetaData metaData, Data data)
        {
            metaData.Title = data.Title;
            metaData.Description = data.Description;
            metaData.DataType = data.Type;
            metaData.AskPasswordEachTime = data.AskPasswordEachTime;
            metaData.LastUpdate = DateTime.Now;
        }
    }
}
