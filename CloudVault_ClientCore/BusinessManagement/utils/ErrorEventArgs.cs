﻿using System;

namespace CloudVault_ClientCore.BusinessManagement.utils
{
    public class ErrorEventArgs : EventArgs
    {
        public Exception Exception { get; }
        public ErrorEventArgs(Exception e)
        {
            Exception = e;
            Console.Error.WriteLine(e.Message);
            Console.Error.WriteLine(e.StackTrace);
        }
    }
}