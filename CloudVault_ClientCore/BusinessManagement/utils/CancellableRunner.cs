﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;

namespace CloudVault_ClientCore.BusinessManagement.utils
{
    public static class CancellableRunner
    {
        public static async Task<ResultRun> ActionAsyncCancellable(
            this object sender,
            CancelEventHandler startEvent,
            EventHandler endEvent,
            ErrorEventHandler errorEvent,
            Func<Task> action)
        {
            var cancelEventsArgs = new CancelEventArgs();
            startEvent?.Invoke(sender, cancelEventsArgs);
            if (cancelEventsArgs.Cancel)
                return ResultRun.Cancelled;
            
            try { await action(); }
            catch (Exception e)
            {
                var errorEventArgs = new ErrorEventArgs(e);
                errorEvent?.Invoke(sender, errorEventArgs);
                return ResultRun.Error;
            }
            
            endEvent?.Invoke(sender, EventArgs.Empty);
            return ResultRun.Success;
        }
        
        public static async Task<(ResultRun, T)> FuncAsyncCancellable<T>(
            this object sender,
            CancelEventHandler startEvent,
            EventHandler endEvent,
            ErrorEventHandler errorEvent,
            Func<Task<T>> function)
        {
            var cancelEventsArgs = new CancelEventArgs();
            startEvent?.Invoke(sender, cancelEventsArgs);
            if (cancelEventsArgs.Cancel)
                return (ResultRun.Cancelled, default);
            
            T result;
            try { result = await function(); }
            catch (Exception e)
            {
                var errorEventArgs = new ErrorEventArgs(e);
                errorEvent?.Invoke(sender, errorEventArgs);
                return (ResultRun.Error, default);
            }
            
            endEvent?.Invoke(sender, EventArgs.Empty);
            return (ResultRun.Success, result);
        }

        public static ResultRun ActionCancellable(
            this object sender,
            CancelEventHandler startEvent,
            EventHandler endEvent,
            ErrorEventHandler errorEvent,
            Action action)
        {
            var cancelEventsArgs = new CancelEventArgs();
            startEvent?.Invoke(sender, cancelEventsArgs);
            if (cancelEventsArgs.Cancel)
                return ResultRun.Cancelled;
            
            try { action(); }
            catch (Exception e)
            {
                var errorEventArgs = new ErrorEventArgs(e);
                errorEvent?.Invoke(sender, errorEventArgs);
                return ResultRun.Error;
            }
            
            endEvent?.Invoke(sender, EventArgs.Empty);
            return ResultRun.Success;
        }
        
        public static (ResultRun, T) FuncCancellable<T>(
            this object sender,
            CancelEventHandler startEvent,
            EventHandler endEvent,
            ErrorEventHandler errorEvent,
            Func<T> function)
        {
            var cancelEventsArgs = new CancelEventArgs();
            startEvent?.Invoke(sender, cancelEventsArgs);
            if (cancelEventsArgs.Cancel)
                return (ResultRun.Cancelled, default);
            
            T result;
            try { result = function(); }
            catch (Exception e)
            {
                var errorEventArgs = new ErrorEventArgs(e);
                errorEvent?.Invoke(sender, errorEventArgs);
                return (ResultRun.Error, default);
            }
            
            endEvent?.Invoke(sender, EventArgs.Empty);
            return (ResultRun.Success, result);
        }
    }
}