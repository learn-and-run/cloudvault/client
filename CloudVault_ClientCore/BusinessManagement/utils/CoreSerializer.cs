﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace CloudVault_ClientCore.BusinessManagement.utils
{
    public static class CoreSerializer
    {
        public static DefaultContractResolver ContractResolver { get; } = new DefaultContractResolver
        {
            NamingStrategy = new CamelCaseNamingStrategy()
        };

        public static string Serialize<T>(T obj)
        {
            return JsonConvert.SerializeObject(obj, obj.GetType(), new JsonSerializerSettings
            {
                ContractResolver = ContractResolver,
                Formatting = Formatting.Indented
            });
        }
        
        public static T Deserialize<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json, new JsonSerializerSettings
            {
                ContractResolver = ContractResolver,
                Formatting = Formatting.Indented
            });
        }
    }
}