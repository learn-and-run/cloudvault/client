﻿using AutoMapper;
using CloudVault_ClientCore.Dbo.metadata;
using CloudVault_ClientCore.Dbo.user;
using CloudVault_ClientCore.Dbo.user.options;
using CloudVault_ClientCore.Dbo.vault;
using CloudVault_ClientCore.Dto.metadata;
using CloudVault_ClientCore.Dto.user;
using CloudVault_ClientCore.Dto.vault;

namespace CloudVault_ClientCore.BusinessManagement.utils
{
    public class CloudVaultMapperProfile : Profile
    {
        public CloudVaultMapperProfile()
        {
            CreateMap<MetaData, MetaDataDto>().ReverseMap();
            CreateMap<User, UserDto>().ReverseMap();
            CreateMap<Vault, VaultDto>().ReverseMap();
            CreateMap<UserOptions, UserOptionsDto>().ReverseMap();
        }

        public static IMapper GenerateDefaultMapper()
        {
            var config = new MapperConfiguration(
                cfg => cfg.AddProfile(new CloudVaultMapperProfile()));
            config.CompileMappings();
            return config.CreateMapper();
        }
    }
}
