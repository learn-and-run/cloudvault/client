﻿using System;
using System.Collections.Generic;

namespace CloudVault_ClientCore.DataAccess.update
{
    public class UpdateBody
    {
        public DateTime? LastSync { get; set; }
        public List<UpdateDataBody> Datas { get; set; }
    }
}