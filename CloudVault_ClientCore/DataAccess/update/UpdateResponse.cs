﻿using System;
using System.Collections.Generic;

namespace CloudVault_ClientCore.DataAccess.update
{
    public class UpdateResponse
    {
        public DateTime LastSync { get; set; }
        public List<UpdateDataResponse> Datas { get; set; }
    }
}