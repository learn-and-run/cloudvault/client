﻿namespace CloudVault_ClientCore.DataAccess.user.login
{
    public class UserLoginBody
    {
        public string Email { get; set; }
        public string Msg { get; set; }
    }
}