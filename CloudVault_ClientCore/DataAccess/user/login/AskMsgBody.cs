﻿namespace CloudVault_ClientCore.DataAccess.user.login
{
    public class AskMsgBody
    {
        public string Email { get; set; }
    }
}