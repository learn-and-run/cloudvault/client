﻿namespace CloudVault_ClientCore.DataAccess.user.login
{
    public class AskMsgResponse
    {
        public string CipherMsg { get; set; }
    }
}