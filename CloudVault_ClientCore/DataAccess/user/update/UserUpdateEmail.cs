﻿namespace CloudVault_ClientCore.DataAccess.user.update
{
    public class UserUpdateEmail
    {
        public string Email { get; set; }
    }
}