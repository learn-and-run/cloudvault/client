﻿namespace CloudVault_ClientCore.DataAccess.user.create
{
    public class UserFinalizeBody
    {
        public string Guid { get; set; }
        public string CipherMsg { get; set; }
    }
}