﻿namespace CloudVault_ClientCore.DataAccess.user.create
{
    public class UserCreateResponse
    {
        public string Guid { get; set; }
        public string Msg { get; set; }
    }
}