﻿namespace CloudVault_ClientCore.DataAccess.user.create
{
    public class UserCreateBody
    {
        public string Email { get; set; }
    }
}