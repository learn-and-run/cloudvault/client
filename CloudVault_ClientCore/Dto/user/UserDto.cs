﻿using CloudVault_ClientCore.Dbo.user.options;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CloudVault_ClientCore.Dto.user
{
    public class UserDto
    {
        public string Email { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public ThemeType UserOptionsThemeType { get; set; }
        public bool UserOptionsStartWithComputer { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public StartupMode UserOptionsStartupMode { get; set; }
        public bool UserOptionsSynchronizeWithServer { get; set; }
        public int UserOptionsDelayForgotPasswordInSecond { get; set; }
    }
}
