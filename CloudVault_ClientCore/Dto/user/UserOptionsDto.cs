﻿using System;
using CloudVault_ClientCore.Dbo.user.options;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CloudVault_ClientCore.Dto.user
{
    public class UserOptionsDto
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public ThemeType ThemeType { get; set; }
        public bool StartWithComputer { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public StartupMode StartupMode { get; set; }
        public bool SynchronizeWithServer { get; set; }
        public int DelayForgotPasswordInSecond { get; set; }
        public DateTime? LastSync { get; set; }
    }
}