﻿using System;
using Newtonsoft.Json;

namespace CloudVault_ClientCore.Dto.vault
{
    public class VaultDto
    {
        //Local Guid needs to be readonly because it is used for GetHashCode()
        public string LocalGuid { get; }
        public long ServerId { get; }

        [JsonConstructor]
        public VaultDto(string localGuid, long serverId)
        {
            LocalGuid = localGuid ??
                             throw new ArgumentNullException(nameof(localGuid));
            ServerId = serverId;
        }

        public override bool Equals(object obj) => LocalGuid.Equals(obj is VaultDto vault ? vault.LocalGuid : obj);

        public override int GetHashCode() => LocalGuid.GetHashCode();
        public override string ToString() => $"VaultDto(LocalGuid={LocalGuid}, ServerId={ServerId})";
    }
}
