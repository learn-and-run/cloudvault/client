﻿using System;
using CloudVault_ClientCore.Dbo.data;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CloudVault_ClientCore.Dto.metadata
{
    public class MetaDataDto
    {
        public string Title { get; set; }
        public string Description { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public DataType DataType { get; set; }
        public DateTime LastUpdate { get; set; }
        public bool AskPasswordEachTime { get; set; }

        [JsonConstructor]
        public MetaDataDto() { }
    }
}
