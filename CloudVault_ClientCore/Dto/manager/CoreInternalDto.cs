﻿using System.Collections.Generic;

namespace CloudVault_ClientCore.Dto.manager
{
    public class CoreInternalDto
    {
        public List<string> KnownUsers { get; set; }
    }
}