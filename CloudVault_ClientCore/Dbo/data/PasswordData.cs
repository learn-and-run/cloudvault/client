﻿namespace CloudVault_ClientCore.Dbo.data
{
    public class PasswordData : Data
    {
        public override DataType Type => DataType.Password;
        private string _description;
        public override string Description
        {
            get => _description ?? (_description = Username);
            set => _description = value;
        }
        public override bool AskPasswordEachTime { get; set; }
        public string Url { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Note { get; set; }

        public override string ToString()
        {
            return $"PasswordData:\n - Title: {Title}\n - Description: {Description}\n" +
                   $" - Url: {Url}\n - Username: {Username}\n - Password: {Password}\n - Note: {Note}";
        }
    }
}
