﻿namespace CloudVault_ClientCore.Dbo.data
{
    public class CreditCardData : Data
    {
        public override DataType Type => DataType.CreditCard;
        private string _description;
        public override string Description
        {
            get => _description ?? (_description = Owner);
            set => _description = value;
        }
        public override bool AskPasswordEachTime { get; set; }
        public string Numbers { get; set; }
        public string Owner { get; set; }
        public string ExpiryDate { get; set; }
        public string Cvv { get; set; }
        public string Note { get; set; }

        public override string ToString()
        {
            return $"CreditCardData:\n - Title: {Title}\n - Description: {Description}\n" +
                   $" - Numbers: {Numbers}\n - Owner: {Owner}\n - ExpiryDate: {ExpiryDate}\n - CVV: {Cvv}\n - Note: {Note}";
        }
    }
}
