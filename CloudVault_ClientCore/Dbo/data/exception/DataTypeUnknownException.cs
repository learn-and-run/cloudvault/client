﻿using System;

namespace CloudVault_ClientCore.Dbo.data.exception
{
    public class DataTypeUnknownException : Exception
    {
        public DataTypeUnknownException() : base("This data type is unknown! Is it null?") { }
    }
}
