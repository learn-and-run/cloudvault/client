﻿using System.Linq;

namespace CloudVault_ClientCore.Dbo.data
{
    public class NoteData : Data
    {
        public override DataType Type => DataType.Note;
        private string _description;
        public override string Description
        {
            get
            {
                if (_description != null || Note == null) return _description;
                var line1 = Note.Split('\r', '\n').FirstOrDefault();
                _description = line1;
                return _description;
            }
            set => _description = value;
        }
        public override bool AskPasswordEachTime { get; set; }
        public string Note { get; set; }

        public override string ToString()
        {
            return $"NoteData:\n - Title: {Title}\n - Description: {Description}\n - Note: {Note}";
        }
    }
}
