﻿namespace CloudVault_ClientCore.Dbo.data
{
    public abstract class Data
    {
        public string Title { get; set; }
        public abstract string Description { get; set; }
        public abstract DataType Type { get; }
        public abstract bool AskPasswordEachTime { get; set; }
    }
}
