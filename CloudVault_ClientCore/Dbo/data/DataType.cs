﻿namespace CloudVault_ClientCore.Dbo.data
{
    public enum DataType
    {
        Password,
        Note,
        CreditCard
    }
}
