﻿using System;
using CloudVault_ClientCore.Dbo.data;
using Microsoft.SqlServer.Server;

namespace CloudVault_ClientCore.Dbo.metadata
{
    public class MetaData : ICloneable
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public DataType DataType { get; set; }
        public DateTime LastUpdate { get; set; }
        public bool AskPasswordEachTime { get; set; }

        // Useful for mapping with AutoMapper
        // ReSharper disable once UnusedMember.Local
        private MetaData() { }

        /// <summary>
        /// Initialize a MetaDataDto from Data
        /// </summary>
        /// <param name="data">The [data] used to generate the MetaDataDto</param>
        public MetaData(Data data)
        {
            Title = data.Title;
            Description = data.Description;
            DataType = data.Type;
            LastUpdate = DateTime.Now;
            AskPasswordEachTime = data.AskPasswordEachTime;
        }

        public object Clone() => MemberwiseClone();
    }
}
