﻿using System;

namespace CloudVault_ClientCore.Dbo.user.options
{
    public class UserOptions
    {
        public ThemeType ThemeType { get; set; }
        public bool StartWithComputer { get; set; }
        public StartupMode StartupMode { get; set; }
        public bool SynchronizeWithServer { get; set; }
        public int DelayForgotPasswordInSecond { get; set; }
        public DateTime? LastSync { get; set; }

        public static UserOptions DefaultUserOptions()
        {
            return new UserOptions()
            {
                DelayForgotPasswordInSecond = 24 * 60 * 60,
                StartupMode = StartupMode.Windowed,
                StartWithComputer = true,
                SynchronizeWithServer = true,
                ThemeType = ThemeType.Dark,
                LastSync = null
            };
        }
    }
}
