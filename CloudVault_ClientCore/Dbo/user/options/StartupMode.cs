﻿namespace CloudVault_ClientCore.Dbo.user.options
{
    public enum StartupMode
    {
        Windowed,
        Minimized,
        Hidden
    }
}
