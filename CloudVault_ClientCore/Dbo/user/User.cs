﻿using CloudVault_ClientCore.Dbo.user.options;

namespace CloudVault_ClientCore.Dbo.user
{
    public class User : IUser
    {
        public string Email { get; set; }
        public UserOptions UserOptions { get; set; }

        public User(string email, UserOptions userOptions = null)
        {
            Email = email;
            UserOptions = userOptions ?? UserOptions.DefaultUserOptions();
        }
    }
}
