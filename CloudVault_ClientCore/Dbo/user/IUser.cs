﻿using CloudVault_ClientCore.Dbo.user.options;

namespace CloudVault_ClientCore.Dbo.user
{
    public interface IUser
    {
        string Email { get; set; }
        UserOptions UserOptions { get; set; }
    }
}
