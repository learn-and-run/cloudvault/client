﻿using System;

namespace CloudVault_ClientCore.Dbo.vault
{
    public class Vault
    {
        //Local Guid needs to be readonly because it is used for GetHashCode()
        public readonly string LocalGuid;
        public long ServerId { get; set; }

        public Vault(string guid) => LocalGuid = guid;
        public Vault(Guid guid) : this(guid.ToString()) { }
        public Vault(string localGuid, long serverId)
        {
            LocalGuid = localGuid;
            ServerId = serverId;
        }

        public override bool Equals(object obj) => LocalGuid.Equals(obj is Vault vault ? vault.LocalGuid : obj);
        public override int GetHashCode() => LocalGuid.GetHashCode();
        public override string ToString() => $"Vault(LocalGuid={LocalGuid}, ServerId={ServerId})";
    }
}
