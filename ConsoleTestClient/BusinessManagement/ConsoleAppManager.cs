﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using CloudVault_ClientCore.BusinessManagement.manager;
using CloudVault_ClientCore.BusinessManagement.user;
using CloudVault_ClientCore.BusinessManagement.vault;
using CloudVault_ClientCore.BusinessManagement.vault.events;
using CloudVault_ClientCore.Dbo.metadata;
using CloudVault_ClientCore.Dbo.vault;
using ConsoleTestClient.BusinessManagement.command;

namespace ConsoleTestClient.BusinessManagement
{
    public partial class ConsoleAppManager
    {
        private CoreManager _coreManager;
        private ConsoleAppInterface _consoleAppInterface;
        private CommandManager CommandManager { get; set; }
        private ConcurrentDictionary<Vault, MetaData> MetaDatas => _coreManager.GetMetaDatas();
        private UserBusiness User => _coreManager.UserBusiness;
        private VaultManager VaultManager => _coreManager.VaultManager;

        public void Init(CoreManager coreManager, ConsoleAppInterface consoleAppInterface)
        {
            _coreManager = coreManager;
            _consoleAppInterface = consoleAppInterface;
            CommandManager = new CommandManager();
            SetupCommands();
            coreManager.Init("https://jeanjacquelin.fr");
            VaultManager.DataDeletedEvent += OnDataDeletedEvent;
            VaultManager.DataAddedEvent += OnDataAddedEvent;
            VaultManager.DataModifiedEvent += OnDataModifiedEvent;
        }

        #region EventListeners
        private static void OnDataModifiedEvent(object sender, DataModifiedEventArgs e)
        {
            Console.WriteLine($"{e.NewMetaData.Title} updated ({e.VaultUpdated})");
        }
        private static void OnDataAddedEvent(object sender, DataAddedEventArgs e)
        {
            Console.WriteLine($"{e.MetaDataAdded.Title} added ({e.VaultAdded})");
        }
        private static void OnDataDeletedEvent(object sender, DataDeletedEventArgs e)
        {
            Console.WriteLine($"{e.MetaDataDeleted.Title} deleted ({e.VaultDeleted})");
        }
        #endregion

        public void ConsoleLoop()
        {
            var stop = false;
            do
            {
                Console.Write("> ");
                try
                {
                    var commandParse = ReadCommand();
                    stop = ExecuteCommand(commandParse);
                }
                catch (CommandParseException parseException)
                {
                    Console.WriteLine("An error occured while parsing :");
                    Console.WriteLine(parseException.Message);
                    Console.WriteLine(parseException.StackTrace);
                }
                catch (Exception e)
                {
                    Console.WriteLine("An error occured during execution :");
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);
                }

            } while (!stop);

            Console.WriteLine();
        }

        private bool ExecuteCommand(CommandParse commandParse)
        {
            switch (commandParse.Name)
            {
                case "quit":
                case "end":
                case "stop":
                case "close":
                case "exit":
                    return true;
            }

            var command = CommandManager.Commands[commandParse.Name];
            if (command == null)
            {
                Console.WriteLine("Unknown command!");
                Console.WriteLine("Type \"help\" to list commands");
            }
            else
                command.ExecuteAction(commandParse.Arguments);
            return false;
        }

        private static CommandParse ReadCommand()
        {
            var arguments = new List<string>();
            var consoleLine = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(consoleLine))
                consoleLine = Console.ReadLine();

            var lineEnumerator = consoleLine.Split(' ');

            if (lineEnumerator.Length < 1)
                throw new CommandParseException("Can't parse command!");

            var name = lineEnumerator[0];

            for (var i = 1; i < lineEnumerator.Length; i++)
            {
                arguments.Add(lineEnumerator[i]);
            }

            return new CommandParse(name, arguments);
        }
    }
}
