﻿using System;
using System.Collections.Generic;

namespace ConsoleTestClient.BusinessManagement.command
{
    public class Command
    {
        public string Name { get; }
        public string[] Arguments { get; }
        public Action<List<string>> ExecuteAction { get; set; }
        public string Description { get; set; }
        public string Help => $"{Name} {string.Join(" ", Arguments)} => {Description}";

        public Command(string name, string description,
            Action<List<string>> action, params string[] arguments)
        {
            Name = name;
            Description = description;
            Arguments = arguments;
            ExecuteAction = action;
        }

        public override bool Equals(object obj)
        {
            return Name.Equals(obj);
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        public override string ToString()
        {
            return $"Command({Help})";
        }
    }
}
