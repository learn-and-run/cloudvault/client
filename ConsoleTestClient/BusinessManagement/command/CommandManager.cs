﻿using System.Collections.Generic;

namespace ConsoleTestClient.BusinessManagement.command
{
    public class CommandManager
    {
        public Dictionary<string, Command> Commands { get; set; }

        public CommandManager()
        {
            Commands = new Dictionary<string, Command>();
        }

        public void AddCommand(Command command)
        {
            Commands.Add(command.Name, command);
        }

    }
}
