﻿using System.Collections.Generic;

namespace ConsoleTestClient.BusinessManagement.command
{
    public class CommandParse
    {
        public string Name { get; }
        public List<string> Arguments { get; }

        public CommandParse(string name, List<string> arguments)
        {
            Name = name;
            Arguments = arguments;
        }
    }
}
