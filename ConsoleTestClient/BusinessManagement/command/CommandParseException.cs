﻿using System;

namespace ConsoleTestClient.BusinessManagement.command
{
    public class CommandParseException : ArgumentException
    {
        public CommandParseException(string message) : base(message) { }
    }
}
