﻿using System;
using System.Collections.Generic;
using System.Linq;
using CloudVault_ClientCore.BusinessManagement.vault;
using CloudVault_ClientCore.Dbo.metadata;
using CloudVault_ClientCore.Dbo.vault;
using ConsoleTestClient.BusinessManagement.command;

namespace ConsoleTestClient.BusinessManagement
{
    public partial class ConsoleAppManager
    {
        private void SetupCommands()
        {
            CommandManager.AddCommand(
                new Command("createUser", "CreateUser", CreateUser));
            CommandManager.AddCommand(
                new Command("knownUsers", "List knownUsers", KnownUsers));
            CommandManager.AddCommand(
                new Command("loadVaults", "Load vaults", LoadVaults));
            CommandManager.AddCommand(
                new Command("loadMetas", "Load MetaDatas", LoadMetaDatas));
            CommandManager.AddCommand(
                new Command("login", "Login", Login));
            CommandManager.AddCommand(
                new Command("connect", "Connect to the server", Connect));
            CommandManager.AddCommand(
                new Command("disconnect", "Disconnect from the server", Disconnect));
            CommandManager.AddCommand(
                new Command("logout", "Logout", Logout));
            CommandManager.AddCommand(
                new Command("sync", "Sync data with the server", Sync));
            CommandManager.AddCommand(
                new Command("list", "List all data", ListData, "[contains]"));
            CommandManager.AddCommand(
                new Command("add", "Add data of type <type>\n    Types: password, note, card", AddData, "<type>"));
            CommandManager.AddCommand(
                new Command("del", "Delete a data", DeleteData, "<dataTitle>"));
            CommandManager.AddCommand(
                new Command("show", "Show a data", ShowData, "<dataTitle>"));
            CommandManager.AddCommand(
                new Command("help", "Show help commands", Help, "[contains]"));
            CommandManager.AddCommand(
                new Command("quit", "Stop the console", _ => { }));
        }

        public async void ShowData(List<string> args)
        {
            if (args.Count != 0)
            {
                var dataPair = MetaDatas.FirstOrDefault(keyPair => keyPair.Value.Title == args[0]);
                if (!dataPair.Equals(default(KeyValuePair<Vault, MetaData>)))
                {
                    var data = await dataPair.Key.GetData(_coreManager, await User.MasterPassword.Get(), dataPair.Value.DataType);
                    Console.WriteLine(data);
                }
                else
                {
                    Console.WriteLine("Data not found");
                }
            }
            else
                Console.WriteLine($"Usage: {CommandManager.Commands["show"].Help}");
        }
        public void DeleteData(List<string> args)
        {
            if (args.Count != 0)
            {
                var data = MetaDatas.FirstOrDefault(keyPair => keyPair.Value.Title == args[0]);
                if (!data.Equals(default(KeyValuePair<Vault, MetaData>)))
                {
                    VaultManager.Delete(data.Key);
                }
                else
                {
                    Console.WriteLine("Data not found");
                }
            }
            else
                Console.WriteLine($"Usage: {CommandManager.Commands["del"].Help}");
        }
        public async void AddData(List<string> args)
        {
            if (args.Count != 0)
            {
                switch (args[0])
                {
                    case "password":
                        await _consoleAppInterface.AddPasswordData();
                        break;
                    case "note":
                        await _consoleAppInterface.AddNoteData();
                        break;
                    case "card":
                        await _consoleAppInterface.AddCreditCardData();
                        break;
                    default:
                        Console.WriteLine($"Unknown type: {args[0]}");
                        break;
                }
            }
            else
                Console.WriteLine("Usage: add <type>\nTypes: password, note, card");
        }
        public void ListData(List<string> args)
        {
            Console.WriteLine($"Total data count = {MetaDatas.Count}, matching with args:");
            foreach (var metaData in MetaDatas
                .Select(keyValue => keyValue.Value)
                .Where(m =>
                    args.All(arg => m.Title.ToLower().Contains(arg.ToLower())))
                .OrderBy(m => m.Title)
            )
            {
                Console.WriteLine($" - {metaData.DataType}({metaData.Title}) {metaData.Description}");
            }
        }
        public void Help(List<string> args)
        {
            Console.WriteLine("Help commands:");
            foreach (var command in CommandManager.Commands
                .Select(keyPair => keyPair.Value)
                .Where(c =>
                    args.All(arg => c.Name.ToLower().Contains(arg.ToLower())))
                .OrderBy(c => c.Name))
            {
                Console.WriteLine($" - {command.Help}");
            }
        }
        
        public async void Login(List<string> args)
        {
            var userBusiness = await _consoleAppInterface.Login();
            await _coreManager.Login(userBusiness);
        }
        
        public async void CreateUser(List<string> args)
        {
            var userBusiness = await _consoleAppInterface.Login();
            await _coreManager.CreateUser(userBusiness);
        }
        
        public void LoadVaults(List<string> args)
        {
            _coreManager.LoadVaults();
        }
        
        public async void LoadMetaDatas(List<string> args)
        {
            await _coreManager.LoadMetaDatas();
        }
        
        public void Logout(List<string> args)
        {
            _coreManager.Logout();
        }
        
        public async void Disconnect(List<string> args)
        {
            await _coreManager.Disconnect();
        }
        
        public async void Connect(List<string> args)
        {
            await _coreManager.Connect();
        }
        
        public async void Sync(List<string> args)
        {
            await _coreManager.SynchronizeWithServer();
        }
        
        public void KnownUsers(List<string> args)
        {
            Console.WriteLine("Known users:");
            foreach (var knownUser in _coreManager.KnownUsers())
            {
                Console.WriteLine($" - {knownUser}");
            }
        }
    }
}
