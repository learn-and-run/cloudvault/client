﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CloudVault_ClientCore.BusinessManagement.manager;
using CloudVault_ClientCore.BusinessManagement.user;
using CloudVault_ClientCore.BusinessManagement.vault;
using CloudVault_ClientCore.Dbo.data;
using CloudVault_ClientCore.Dbo.user;

namespace ConsoleTestClient.BusinessManagement
{
    public class ConsoleAppInterface : IPasswordAsker
    {
        private readonly CoreManager _coreManager;
        private VaultManager VaultManager => _coreManager.VaultManager;
        private UserBusiness User => _coreManager.UserBusiness;


        public ConsoleAppInterface(CoreManager coreManager)
        {
            _coreManager = coreManager;
        }

        public async Task<UserBusiness> Login()
        {
            var email = AskUsername();
            var password = await AskPassword();
            var user = new User(email);
            return new UserBusiness(user, this, password);
        }

        public static string AskUsername()
        {
            Console.WriteLine(@"Please enter your email:");
            return Console.ReadLine();
        }

        public Task<string> AskPassword()
        {
            Console.WriteLine(@"Please enter a master password:");
            var sb = new StringBuilder();
            ConsoleKeyInfo keyInfo;
            while ((keyInfo = Console.ReadKey(true)).Key != ConsoleKey.Enter)
                sb.Append(keyInfo.KeyChar);
            return Task.FromResult(sb.ToString());
        }


        public async Task AddPasswordData()
        {
            var results = Ask("Title:", "Description:", "Url:", "Note:", "Username:");
            var password = new PasswordData()
            {
                Title = results[0],
                Description = results[1],
                Url = results[2],
                Note = results[3],
                Username = results[4],
                Password = ConsoleAskPassword()
            };
            await VaultManager.AddData(password, await User.MasterPassword.Get());
        }
        public async Task AddNoteData()
        {
            var results = Ask("Title:", "Description:", "Note:");
            var note = new NoteData()
            {
                Title = results[0],
                Description = results[1],
                Note = results[2]
            };
            await VaultManager.AddData(note, await User.MasterPassword.Get());
        }
        public async Task AddCreditCardData()
        {
            var results = Ask("Title:", "Description:", "Numbers:", "Owner:", "ExpiryDate:", "CVV:", "Note:");
            var card = new CreditCardData()
            {
                Title = results[0],
                Description = results[1],
                Numbers = results[2],
                Owner = results[3],
                ExpiryDate = results[4],
                Cvv = results[5],
                Note = results[6]
            };
            await VaultManager.AddData(card, await User.MasterPassword.Get());
        }

        #region Utils
        private static List<string> Ask(params string[] what)
        {
            var results = new List<string>();
            foreach (var s in what)
            {
                Console.WriteLine(s);
                results.Add(Console.ReadLine());
            }
            return results;
        }

        private static string ConsoleAskPassword()
        {
            Console.WriteLine("Password:");
            var sb = new StringBuilder();
            ConsoleKeyInfo keyInfo;
            while ((keyInfo = Console.ReadKey(true)).Key != ConsoleKey.Enter)
                sb.Append(keyInfo.KeyChar);
            return sb.ToString();
        }
        #endregion

    }
}
