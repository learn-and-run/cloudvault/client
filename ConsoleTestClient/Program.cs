﻿using System;
using CloudVault_ClientCore.BusinessManagement.manager;
using ConsoleTestClient.BusinessManagement;

namespace ConsoleTestClient
{
    public class Program
    {
        /// <summary>
        /// Permet de manipuler CloudVault avec une console
        /// </summary>
        public static void Main(string[] args)
        {
            Console.WriteLine("Welcome to CloudVault console!\n");
            var coreManager = new CoreManager();
            var appInterface = new ConsoleAppInterface(coreManager);
            var appManager = new ConsoleAppManager();
            appManager.Init(coreManager, appInterface);

            appManager.ConsoleLoop();
        }
    }
}
